
from grainite_cl.api.grainite_client import get_client
from grainite_cl.api.grainite_client import Grainite
from grainite_cl.api.value import Value
from grainite_cl.api.event import Event
from grainite_cl.api.grainite_exception import GrainiteException
from grainite_cl.api.grain import Grain
from grainite_cl.api.table import Table
from grainite_cl.api.topic import Topic

import concurrent
import argparse

# connect to grainite and get handle to the customer_table
parser = argparse.ArgumentParser(description="Grainite hello world program for Python")
parser.add_argument('--server', default='localhost', type=str, help='Grainite IP address')
args=parser.parse_args()
 
grainite: Grainite = get_client(args.server, 5056)
table: Table = grainite.get_table(app_name="helloPython", table_name="person_table")

# Get or create a grain
# A grain is a fundamental abstraction in grainite. Here we create grains representing
# three people
# Mr Musk
customerGrain: Grain = table.get_grain(Value("Elon"))
# This is a sync call. 
# Value is a Grainite abstraction for a typed variant type. Here 
# it holds a string
customerGrain.set_value(Value("Musk"))

# Mr Bezos
customerGrain = table.get_grain(Value("Jeff"))
# This time, we use an async call to set the value. An async call returns a future
# and optionally, can be given a callback to fire when the operation finishes. 
fut = customerGrain.set_value_async(Value("Bezos"))
# We will immediately wait on this future here
fut.result

# Mr Gates
customerGrain = table.get_grain(Value("Bill"))
# This time, we use an async call and also supply a callback to know when the op is finished

class Callback:
  @staticmethod
  def on_completion(resp, exception):
    if resp:
        print("Completed op")
    else:
        raise exception

# Note the .result at the end. This causes us to wait till the future has a completion.    
customerGrain.set_value_async(Value("Gates"), callback = Callback()).result

# Now all the grains are setup and have a value. We will now send three events 
# to a topic, one each for these three people. Take a look at the 
# app config in the app.py.yaml file. Notice that events sent to the people topic
# will automatically be processed by the GreetingHandler action handler

# First get a handle to the topic
topic: Topic = grainite.get_topic("helloPython", "people")

# Now send the three events
futs = []
for user in ["Elon", "Jeff", "Bill"]:    
    futs.append(topic.append_async(Event(key=Value(user), payload=Value("says hi"))))

# Wait for the event appends to finish
concurrent.futures.wait(futs)

print("Appended three events")

# At this point, the action handler is going to kick in. Take a look at the action/GreetingHandler.py
# code. For every input sent to the people topic, it will write a corresponding event
# to the greetings topic
# You can use the gx utility to inspect topics and tables. run gx topic dump and dump both the topics. 
# You can also run gx table dump and dump the three grains above.
# You can als tail a topic by doing a gx topic dump -f 
# 
