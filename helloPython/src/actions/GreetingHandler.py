from grainite_cl.api.context import action, grain_context
from grainite_cl.api.value import Value

def handleGreeting(act: action.TopicEvent, context: grain_context.GrainContext):

    # In this app we know that handleGreeting will only be processing TopicEvents.
    # But in general, an action handler can process any action. Take a look at action.py
    # to see the different kinds of actions that can be handled by action handlers

    # The context object is a handle for the grain that is processing this event
    # It has methods to get the blob value or any of the map values from the grain state
    # It also has methods to set the grain state or communicate with other grains by
    # sending them G2G messages or to append events to a topic or to start a timer
    # for deferred work

    # Logs show up in the configured app log file - Usually gxapps.log
    context.get_logger().info(f'Received event for key - {context.get_key()}')

    # Create an output event based on the grain key, grain state and topic event payload
    first_name = context.get_key().as_string()
    last_name = context.get_value().as_string()
    greeting = act.payload.as_string()
    msg = f'{first_name} {last_name} {greeting}'

    # Append this event to the greetings topic.
    context.send_to_topic("greetings", key=context.get_key(), event=Value(msg))

    # Return a success status. Note that in the general case, any state modifications
    # or output actions (like G2G messages to be sent or topic appends) will all 
    # execute atomically when a success status is returned. If an exception is thrown
    # none of the aftereffects will be applied
    # Please consult grainite conceptual docs or reach out to use to understand 
    # the semantics offered by Grainite here
    return action.ActionResult.Success(act)

