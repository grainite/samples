# helloPython 

**helloPython** is a hello world program for the Grainite Python API. It shows how to write a program that appends events to a topic, then proceses these events in an action handler where they are augmented with the key state and written back to another topic. It assumes familiarity with Grainite concepts. Also refer to the inline comments in `src/Client.py` and `src/actions/GreetingHandler.py` files

Note: If you haven't already, please follow setup steps 1-3 in the [**the quickstart guide**](../quickstart.md) to set up the gx CLI. If running Grainite locally on Docker, please also follow the instructions in that file for setting up the Docker container.

## Prerequisites
- Python 3.7 or later
- Grainite client package. To install:
  `pip install grainite-cl --index-url https://gitlab.com/api/v4/projects/41132986/packages/pypi/simple`


## Deploy helloPython onto Grainite

`gx` reads the application YAML file to parse what tables and topics are needed. This information is then sent to the Grainite server to deploy the application.

```bash
gx load -H <server host>
```

Note: By default, gx will look for a YAML configuration file in the current directory. If one is not found or is not valid, gx will throw an exception for most commands. To specify a particular YAML configuration file, you can use **`gx <sub command(s)> -c <path to config file>`** .

## Run helloPython 

```bash
python3 src/Client.py [--server=<ipaddr>]
```

## Observing helloPython 

To see what happened, you can look at a couple of things:

1. App log
2. Events in a Topic
3. Grains in a Table

### 1. App Log

The following will print the application logs.

```bash
gx log --follow
```

### 2. Events in a Topic

Run the below command. `gx` will then ask you to select a topic in the application and will print the events in that topic.

```bash
gx topic dump
```

### 3. Grains in a Table

Run the below command. `gx` will then ask you to select a table in the application and will print the grains in that table.

```bash
gx table dump
```

<br><br>

