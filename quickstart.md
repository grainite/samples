# For Users Running Grainite on Docker

## Prerequisites
- Java: [Link to instructions for installing JDK](https://gitbook.grainite.com/developers/getting-started/environment-setup#java)
- [Docker Desktop](https://docs.docker.com/get-docker/)

## System Requirements

In addition to any system requirements specified by Docker, your machine should meet the following requirements to properly run Grainite inside the Docker container and the resources must be configured as such in the Preferences panel of Docker Desktop (click on the gear icon in the top right of the window):

- 4 physical CPU cores
- 4 GB of RAM
- 1 GB of available Swap space
- 32 GB of available disk space

## Setup

1. If you have not done so already, first clone the samples repository onto your machine.
    ```bash
    git clone https://gitlab.com/grainite/samples && cd samples
    ```

2. Create an alias for the included GX wrapper script. GX is the Grainite CLI. Once you complete this setup and decide to build your own app, it is recommended to install the GX binary locally via the [GX CLI setup instructions in our documentation](https://gitbook.grainite.com/developers/getting-started/environment-setup#gx-cli).
    ```bash
    # Declare an alias to use gx; or add $(pwd)/gx to your $PATH for ease of use
    alias gx=$(pwd)/gx
    ```

3. Then, compile all sample applications and download necessary dependencies.
    ```bash
    ./mvnw clean compile package
    ```

4. Run the below command to download the Grainite server docker image and start running it. If you are developing an application for a cluster running a specific known version, you should replace `latest` with the Grainite version of the cluster you'll be deploying to (e.g. `2307`).
    ```bash
    gx start --image quay.io/grainite/single:latest
    ```


## Running Applications

5. You are now ready to connect to your newly launched Grainite server running on Docker and load and run a sample application. Make sure to pass in the IP address of your machine (`localhost` if running locally) in place of {SERVER IP}.

    ```bash
    cd wordcount

    # Load the application on Grainite
    # Note: GX is an alias set to samples/gx
    gx load 

    # Submit traffic
    ./run.sh {SERVER IP} default load resources/sample1.txt

    # Get the word count for 'this'
    ./run.sh {SERVER IP} default word this

    # Get the word count for all words
    ./run.sh {SERVER IP} default allwords
    ```

## Next Steps
6. To learn more about this app and see how it's working, check out the [`pt0-intro.md`](./wordcount/tutorial/pt0-intro.md) file in the `samples/wordcount/tutorial` directory and browse the source code under `samples/wordcount/src`. There's also another sample application called Userflows under `samples/userflows` which showcases how Grainite can be used to build applications with real-time user interaction and analytics.

    Grainite documentation is available under `samples/docs/` in both PDF (`docs.pdf`) and markdown (`docs.md`). The javadoc for our API can be browsed by pointing your web browser to the absolute path of the `samples/javadoc/index.html` file. If you have any questions, don't hesitate to reach out to your Grainite representative for a fast response or drop us an email at eval@grainite.com.

<br/><br/>

# For Hosted Trial Users
This section of this quickstart file is for users of the Grainite Hosted Trial. For users who will self-host Grainite in a docker container, you can instead refer to the [Running Grainite on Docker section](#for-users-running-grainite-on-docker).

## Prerequisites
- Java: [Link to instructions for installing JDK](https://gitbook.grainite.com/developers/getting-started/environment-setup#java)

## Setup

1. If you have not done so already, first clone the samples repository onto your machine and navigate into the certs directory.
    ```bash
    git clone https://gitlab.com/grainite/samples
    cd samples/certs
    ```

2. Next, download the client certificate package that was emailed to you by the Grainite team (`client_certs.tar.gz`). Replace {PATH TO CERTS} in the command below with the path to the newly downloaded `client_certs.tar.gz`.
    ```bash
    # Example: tar xvf ~/Downloads/client_certs.tar.gz
    tar xvf {PATH TO CERTS}
    ```

3. `cd` back up to the samples directory and create an alias for gx, which is the Grainite CLI.
    ```bash
    # Declare an alias to use gx; or add $(pwd)/gx to your $PATH for ease of use
    alias gx=$(pwd)/gx
    ```

4. The final setup step is to compile all sample applications and download necessary dependencies.
    ```bash
    ./mvnw clean compile package
    ```

## Running Applications

5. You are now ready to connect to the Grainite trial server and load and run a sample application. Make sure to pass in the IP address provided to you by the Grainite team in place of {SERVER IP}.

    ```bash
    cd wordcount

    # Load the application on Grainite
    # Note: GX is an alias set to samples/gx
    gx load 

    # Submit traffic
    ./run.sh {SERVER IP} default load resources/sample1.txt

    # Get the word count for 'this'
    ./run.sh {SERVER IP} default word this

    # Get the word count for all words
    ./run.sh {SERVER IP} default allwords
    ```

## Next Steps
6. To learn more about this app and see how it's working, check out the [`pt0-intro.md`](./wordcount/tutorial/pt0-intro.md) file in the `samples/wordcount/tutorial` directory and browse the source code under `samples/wordcount/src`. There's also another sample application called Userflows under `samples/userflows` which showcases how Grainite can be used to build applications with real-time user interaction and analytics.

    Grainite documentation is available under `samples/docs/` in both PDF (`docs.pdf`) and markdown (`docs.md`). The javadoc for our API can be browsed by pointing your web browser to the absolute path of the `samples/javadoc/index.html` file. If you have any questions, don't hesitate to reach out to your Grainite trial representative for a fast response or drop us an email at eval@grainite.com.

<br/><br/>
