# Application Configuration

First, you will define our WordCount application by creating a YAML configuration file. You will create an app directory called `my-wordcount` and a config file called `app.yaml` by running the following commands.

```bash
mkdir my-wordcount && cd my-wordcount
touch app.yaml
```

Next, you will define the name of the app, package ID, and the location of the jar file for WordCount. Add the following:

```yaml
app_name: wordcount
package_id: org.sample
# Location of jar file relative to this config file
jars: 
    - target/wordcount-jar-with-dependencies.jar
```

Note: The **JAR** file will need to contain all the dependencies of the application. There are instructions to create the JAR further into this guide.

Grainite can manage _lots_ of topics but for this application, you will define just one:

```yaml
app_name: wordcount
package_id: org.sample
# Location of jar file relative to this config file
jars: 
    - target/wordcount-jar-with-dependencies.jar 

topics:
-   topic_name: line_topic
    key_name: hash
    key_type: string
```

This defines a topic called `line_topic` with the key `hash` of type `string`.

The `topic_name` uniquely identifies a topic within the app, the `key_name` provides a way for grains to subscribe to topics, while `key_type` defines the type of the key. The value for `key_type` is not limited to a primitive data type (string, int, long, etc) but can be anything that describes the key name (userId, username, buyerId, etc.).

Finally, you will define three tables for the application:

1. Line Table
2. Word Stats Table
3. Doc Stats Table

You will do this by adding the following to the configuration file.

```yaml
app_name: wordcount
package_id: org.sample
# Location of jar file relative to this config file
jars:
  - target/wordcount-jar-with-dependencies.jar

topics:
  - topic_name: line_topic
    key_name: hash
    key_type: string

tables:
  - table_name: line_table
    key_type: string
    action_handlers:
      - type: java
        class_name: org.sample.wordcount.LineEventHandler
        actions:
          - action_name: handleLineEvent
            subscriptions:
              - subscription_name: lineUpdates
                topic_name: line_topic
                topic_key: hash

  - table_name: word_stats_table
    key_type: string
    maps:
      - id: 0
        name: words count
        key_type: string
        value_type: long
    action_handlers:
      - type: java
        class_name: org.sample.wordcount.WordStatsHandler
        actions:
          - action_name: handleWordEvent

  - table_name: doc_stats_table
    key_type: string
    action_handlers:
      - type: java
        class_name: org.sample.wordcount.DocStatsHandler
        actions:
          - action_name: handleDocStatsEvent
```

Each table has a `table_name`, `key_type` , `value_type` and `action_handlers`.

The `table_name` uniquely identifies a table, the `key_type` and `value_type` define the type of key and value for each grain in the table, and `action_handlers` contain definitions for handlers in each grain, in the table.

Tables can also have `maps` to define the type of data contained in each map. Each can have an `id`, `name`, `key_type` and `value_type`. The `id` and `name` are used to identify the map, while `key_type` and `value_type` are used to define the type of key and value for each entry in the map.

An **Action Handler** has a `type`, `actions` and some additional properties based on the provided type. The type defines the language that the action handler is written in:

```yaml
-   type: java
    class_name: org.sample.wordcount.LineEventHandler
    actions:
    -   action_name: handleLineEvent
        subscriptions:
        - subscription_name: lineUpdates
          topic_name: line_topic
          topic_key: hash
```

Java action handlers require a `class_name` which is the full class name of the defined Java class. These classes should be part of the project package (defined by `package_id`).


**Actions** define the type of work that the action handlers are expected to handle. Each action is tied to a method in the action handler. For example, in WordCount, `handleLineEvent` is the name of the action as well as the method in `LineEventHandler`. It is possible to define a different method name for an action by specifying `method_name` , but you will not be doing that for this application.

**Subscriptions** allow for action handlers to listen to a topic and stream events from it. As a result, when a new event is appended to a topic, all subscriptions for that topic are triggered and the method corresponding to the subscription is invoked. For example, in WordCount, when an event is appended to `line_topic`, the event will be streamed to `line_table` as part of the `lineUpdates` subscription.

<br>

## [Next:](./pt3-application-structure.md)
### [Part 3 – Application Structure](./pt3-application-structure.md)

<br>
<br>

### [Previous:](./pt1-gx-setup.md)
#### [Part 1 – GX Setup](./pt1-gx-setup.md)