# Running the Application

## Step 0: Package your application

First, you will need to create JAR with our application and its dependencies. You can do this by running the following command in the top of the project directory.

```bash
mvn package
```

This will create the JAR file `target/wordcount-jar-with-dependencies.jar` which you can use to upload to Grainite. *Note: If you make any changes to your application after packaging, you'll need to re-run the above command or `mvn clean package`.*

## Step 1: Load your application onto Grainite

### Step 1.1: Replace existing sample WordCount app running on Grainite

You can check if an app with the same name is already loaded onto Grainite, for example, if you previously tested the pre-built WordCount app before building your own in this tutorial. Running the following GX command:

```bash
$> gx app ls
Running gx app list with options:
Admin Port: 5057
      Host: <SERVER IP>

1. default:wordcount
```

We see that the pre-built app called 'wordcount' is already loaded. By default, your application will have the same name, as specified in the YAML file. You should first remove the currently loaded app with:

```bash
$> gx app rm -a wordcount
Running gx app remove with options:
Environment: default
   App Name: wordcount
 Admin Port: 5057
       Host: <SERVER IP>

[INFO] Sent app deletion request.
...
[SUCCESS] Application deleted.
```

### Step 1.2: Load your packaged app onto Grainite
With the JAR file you created above, you can use the following command to load your app onto Grainite.

*Note: `gx load` uses `localhost` as the default host server IP. If Grainite has provided you with a trial server instance, you will need to specify it as the host: Run the `gx load -H <SERVER IP>` command below, using the IP address given to you.*

```bash
# This command will run 'gx load' with 'localhost' as the default host.
gx load
```

```bash
$> gx load -H <SERVER IP> 
Running gx load with options:
      Config: ./app.yaml
         Jar: /wordcount/./target/wordcount-jar-with-dependencies.jar
        Host: <SERVER IP>
        Port: 5057

[INFO] Validating code files.
[INFO] Loading application.
 ...
[SUCCESS] Application deployed and ready.
```

## Step 2: Run the application

Now that your application is loaded onto Grainite, you can now run WordCount commands through the client. First, you'll need a text file to pass to the client as input. You can use the following files from `samples/wordcount/resources`:

- [sample1.txt](../resources/sample1.txt)
- [sample2.txt](../resources/sample2.txt)

To run the client application and push events into Grainite, you can use the [`run.sh`](../templates/run.sh) script (Not the run.sh from the parent directory). Copy `run.sh` into your `my-wordcount` directory.

First, set the new script as executable.
```bash
$> chmod +x run.sh
```

 Now, you can load some input data onto your app:
```bash
$> ./run.sh load ../resources/sample1.txt
```

Now that you have loaded some data, you can perform other actions on it from the client API you wrote in the [previous step](./pt5-client-api.md).

```bash
# Get the word count of 'this'
$> ./run.sh word this
this: 5
# Get the count of all words alphabetically
$> ./run.sh allwords
       1: five
       1: four
       5: is
       1: one
       5: sentence
       5: this
       1: three
       1: two
# Get the document statistics for 'sample1.txt'
$> ./run.sh doc sample1.txt
Document sample1.txt { Sentences: 5, Words: 20 }
```

Congrats! You've completed the WordCount tutorial and learned how to build a Grainite application from scratch.

<br>

### [Previous:](./pt5-client-api.md)
#### [Part 5 – WordCount Client](./pt5-client-api.md)