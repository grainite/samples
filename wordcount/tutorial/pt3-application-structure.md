---
description: Java code generation
---

# Application Structure

**GX** makes it easy to get started writing the application by generating classes for you, based on the configuration file. You can do this in your `my-wordcount` app directory, where you  created an `app.yaml` file in the previous step, by running `gx gen all -M`.

Note: Currently, **`gx gen all` only supports pure Java projects**. For others, files will have to be created manually.

```bash
$> cd my-wordcount
$> gx gen all -M
Running gx gen all with options:
  Path: .
 Force: false
Modify: true
Config: ./app.yaml

[INFO] Generating Constants
[INFO] Upserting class Constants
...
[SUCCESS] Generated classes and methods
```

Optional: If you have the `tree` utility installed on your machine, you can then visualize the application structure.

```bash
$> tree
.
├── app.yaml
├── pom.xml
└── src
    └── main
        └── java
            └── org
                └── sample
                    └── wordcount
                        ├── Client.java
                        ├── Constants.java
                        ├── DocStatsHandler.java
                        ├── LineEventHandler.java
                        └── WordStatsHandler.java
```


Along with the **action handler classes**, **a constants class**, **a client class**, and the **project build file** have also been **generated**.

`Client.java` contains an example for pushing an event to a topic. For WordCount, you will modify this file to read sentences from a text file and push them as events into the `line_topic`. `Constants.java` contains variables for actions, subscriptions tables, and topics.

In the next step, you will modify these generated files with the business logic for WordCount.

<br>

## [Next:](./pt4-implement-application-logic.md)
### [Part 4 – Implement Application Logic](./pt4-implement-application-logic.md)

<br>
<br>

### [Previous:](./pt2-application-configuration.md)
#### [Part 2 – Application Configuration](./pt2-application-configuration.md)
