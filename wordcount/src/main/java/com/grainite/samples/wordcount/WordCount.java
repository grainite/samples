package com.grainite.samples.wordcount;

import com.github.javafaker.Book;
import com.github.javafaker.Faker;
import com.grainite.api.Callback;
import com.grainite.api.Event;
import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.GrainiteException;
import com.grainite.api.GrainiteException.ErrorType;
import com.grainite.api.Key;
import com.grainite.api.KeyValue;
import com.grainite.api.MapQuery;
import com.grainite.api.RecordMetadata;
import com.grainite.api.Table;
import com.grainite.api.Topic;
import com.grainite.api.Value;
import com.grainite.samples.wordcount.handlers.LineEventHandler;
import com.grainite.samples.wordcount.model.DocumentStats;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class WordCount {
  final static byte[] MAX_KEY = new byte[] {(byte) 255, 0};

  private static void usage() {
    System.err.println(
        "Usage: wordcount < ca cert > < client cert > < client key > < host IP > < env name > < cmd > [ params ]");
    System.err.println("where commands are:");
    System.err.println("     load: load <path to document>");
    System.err.println("  genload: genload <num documents> <num sentences per doc>");
    System.err.println("     word: word <word>");
    System.err.println(" allwords: allwords");
    System.err.println("      doc: doc <document name>\n");
    System.err.println(
        "NOTE: < ca cert >, < client cert >, < client key > are automatically passed when invoked via run.sh.");

    System.exit(1);
  }

  public static void main(String[] args) throws Exception {
    if (args.length < 6) {
      // tls certs, keys, ip, env name, and command are required to be passed.
      usage();
    }

    File caCert = Paths.get(args[0]).toFile();
    File clientCert = Paths.get(args[1]).toFile();
    File clientKey = Paths.get(args[2]).toFile();

    String host = args[3];
    String envName = args[4];

    Grainite client = null;

    if (caCert.exists() && clientCert.exists() && clientKey.exists()) {
      client = GrainiteClient.getClient(host, 5056, caCert.getAbsolutePath(),
          clientCert.getAbsolutePath(), clientKey.getAbsolutePath(),
          envName == null ? "default" : envName);
    } else {
      client = GrainiteClient.getClient(host, 5056, envName == null ? "default" : envName);
    }

    String cmd = args[5];

    switch (cmd.toLowerCase()) {
      case "load":
        new WordCount().load(client, args[6]);
        break;
      case "word":
        new WordCount().getWordCount(client, args[6]);
        break;
      case "doc":
        new WordCount().getDocumentStats(client, args[6]);
        break;
      case "allwords":
        new WordCount().getAllWordsStats(client);
        break;
      case "genload":
        new WordCount().genload(client, args[6], args[7]);
        break;
      default:
        usage();
    }

    client.close();
  }

  private void genload(Grainite client, String numDocs, String numSentences) throws Exception {
    int docCount = Integer.parseInt(numDocs);
    int sentenceCount = Integer.parseInt(numSentences);

    Faker faker = Faker.instance();

    Book book = faker.book();

    Topic topic = client.getTopic(Constants.APP_NAME, Constants.LINE_TOPIC);

    AtomicLong totalLines = new AtomicLong(0);
    AtomicLong failures = new AtomicLong(0);

    for (int i = 0; i < docCount; i++) {
      String title = book.title();
      System.out.println("Sending " + sentenceCount + " sentences for: " + title);
      for (int j = 0; j < sentenceCount; j++) {
        String line = faker.lorem().sentence(10, 10);
        topic.appendAsync(new Event(Value.of(line.substring(0, 2)),
                              Value.ofObject(new LineEventHandler.LineEvent(title, line))),
            new Callback<RecordMetadata>() {
              @Override
              public void onCompletion(RecordMetadata completion, Exception exception) {
                totalLines.incrementAndGet();
                if (exception != null) {
                  failures.incrementAndGet();
                }
              }
            });
      }
    }

    topic.close();

    System.out.println("Sent " + totalLines.get() + " sentences across " + docCount
        + " documents, with " + failures.get() + " failures");
  }

  private void getAllWordsStats(Grainite client) throws Exception {
    // display sorted results by going from 0 to 255 as the key for the words
    // table.
    List<String> allWordsList = new ArrayList<>();

    Table wordStats = client.getTable(Constants.APP_NAME, Constants.WORDS_TABLE);

    for (Iterator<Grain> iter = wordStats.scan(); iter.hasNext();) {
      Grain g = iter.next();

      // scan through all words in this bucket and print sorted
      Iterator<KeyValue> it = g.mapQuery(MapQuery.newBuilder()
                                             .setMapId(0)
                                             .setRange(Key.minKey(), true, Key.maxKey(), true)
                                             .build());
      while (it.hasNext()) {
        KeyValue kv = it.next();
        allWordsList.add(String.format("%8d: %s", kv.getValue().asLong(), kv.getKey().asString()));
      }
    }

    allWordsList.stream()
        .sorted((x, y) -> x.split(":")[1].compareTo(y.split(":")[1]))
        .forEach(System.out::println);
  }

  private void getDocumentStats(Grainite client, String docName) throws Exception {
    Table docTable = client.getTable(Constants.APP_NAME, Constants.DOC_STATS_TABLE);
    Grain docGrain = docTable.getGrain(Value.of(docName), false);
    if (docGrain == null) {
      System.out.printf("Document %s not found\n", docName);
      System.exit(1);
    }

    DocumentStats stats = docGrain.getValue().asType(DocumentStats.class);

    System.out.printf(
        "Document %s { Sentences: %d, Words: %d }\n", docName, stats.numPeriods, stats.numWords);
  }

  private void getWordCount(Grainite client, String word) throws GrainiteException {
    Table wordTable = client.getTable(Constants.APP_NAME, Constants.WORDS_TABLE);
    Grain g = wordTable.getGrain(Value.of(word.substring(0, 1)), false);

    if (g == null) {
      System.out.printf("Word %s not found\n", word);
      System.exit(1);
    }

    try {
      System.out.printf("%s: %d\n", word, g.mapGet(0, Value.of(word)).asLong(0));
    } catch (GrainiteException ex) {
      if (ex.getErrorType() == ErrorType.ERRNO_KZ_NOEXIST) {
        System.err.printf("Word %s not found\n", word);
        System.exit(1);
      }

      ex.printStackTrace(System.err);
    }
  }

  private void load(Grainite client, String inputFile) throws Exception {
    // Get topic sentences_topic.
    Topic topic = client.getTopic(Constants.APP_NAME, Constants.LINE_TOPIC);
    Path inputPath = Paths.get(inputFile);
    AtomicBoolean didEncounterFailure = new AtomicBoolean(false);
    AtomicLong numEventsSent = new AtomicLong(0);
    AtomicLong numEventCallbacksReceived = new AtomicLong(0);
    System.out.println("Reading file and sending to the topic.");
    Files.lines(inputPath).forEach(line -> {
      if (!line.isEmpty()) {
        numEventsSent.incrementAndGet();
        topic.appendAsync(new Event(Value.of(line.substring(0, 2)),
                              Value.ofObject(new LineEventHandler.LineEvent(
                                  inputPath.getFileName().toString(), line))),
            new Callback<RecordMetadata>() {
              @Override
              public void onCompletion(RecordMetadata completion, Exception exception) {
                numEventCallbacksReceived.incrementAndGet();
                if (exception != null) {
                  didEncounterFailure.set(true);
                }
              }
            });
      }
    });

    topic.flush();

    while (numEventsSent.get() != numEventCallbacksReceived.get()) {
      // Wait for all events to be sent to the topic.
      Thread.sleep(100);
    }

    if (didEncounterFailure.get()) {
      System.err.println("[ERROR] Encountered an error while sending events to the topic.");
      System.exit(1);
    } else {
      System.out.println("[SUCCESS] File was read and all events were sent to the topic.");
    }
  }
}
