package com.grainite.samples.wordcount.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.GrainRequest;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import com.grainite.samples.wordcount.model.DocumentStats;

public class DocStatsHandler {
  public ActionResult handleDocStatsEvent(Action action, GrainContext context) {
    DocumentStats prevStats = context.getValue().asType(DocumentStats.class);
    DocumentStats event =
        ((GrainRequest)action).getPayload().asType(DocumentStats.class);

    context.getLogger().info("DocStatsHandler: Received wordCount for doc - " +
                             context.getKey().asString());

    prevStats.numPeriods += event.numPeriods;
    prevStats.numWords += event.numWords;

    context.setValue(Value.ofObject(prevStats));
    return ActionResult.success(action);
  }
}
