package com.grainite.samples.wordcount.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.TopicEvent;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import com.grainite.api.context.GrainContext.GrainOp;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Stream;
import com.grainite.samples.wordcount.Constants;
import com.grainite.samples.wordcount.model.DocumentStats;

/**
 * This class was generated by GX.
 * <p>
 * Table: line_table Key Type: string
 */
public class LineEventHandler {
  /**
   * Handle a new line sent by the client.
   * <p>
   * Breaks up the line into words, counts periods to compute sentence count, and then pushes to the
   * word and document stats tables.
   */
  public ActionResult handleLineEvent(Action action, GrainContext context) {
    LineEvent event = ((TopicEvent) action).getPayload().asType(LineEvent.class);

    final AtomicLong wordCount = new AtomicLong(0);
    Stream.of(event.getLine().split("[. ]+")).forEach(w -> {
      // for each word, send to word stats table, and increment document count.
      if (w.trim().length() == 0) return;

      w = w.toLowerCase();
      wordCount.incrementAndGet();
      GrainOp.Invoke invoke = new GrainOp.Invoke(Constants.HANDLE_WORD_EVENTS_ACTION, Value.of(w));
      context.sendToGrain(Constants.WORDS_TABLE, Value.of(w.substring(0, 1)), invoke, null);
    });

    long numPeriods = countPeriods(event.getLine());

    GrainOp.Invoke invokeDoc = new GrainOp.Invoke(Constants.HANDLE_DOC_STATS_EVENT,
        Value.ofObject(new DocumentStats(numPeriods, wordCount.get())));

    // now update document count.
    context.sendToGrain(Constants.DOC_STATS_TABLE, Value.of(event.getDocName()), invokeDoc, null);

    return ActionResult.success(action);
  }

  private int countPeriods(String line) {
    int count = 0;
    int nextIndex = -1;

    while ((nextIndex = line.indexOf('.', nextIndex + 1)) != -1) {
      count++;
    }
    return count;
  }

  /**
   * Event payload in `line_topic`.
   */
  public static class LineEvent implements Serializable {
    private String docName;
    private String line;

    public LineEvent() {}

    public LineEvent(String docName, String line) {
      this.docName = docName;
      this.line = line;
    }

    public String getLine() {
      return line;
    }

    public String getDocName() {
      return docName;
    }

    @Override
    public String toString() {
      return "LineEvent{"
          + "docName='" + docName + '\'' + ", line='" + line + '\'' + '}';
    }
  }
}
