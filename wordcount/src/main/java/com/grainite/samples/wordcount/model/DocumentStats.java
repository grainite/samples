package com.grainite.samples.wordcount.model;

import java.io.Serializable;

/**
 * Structure to store in `doc_stats_table`.
 */
public class DocumentStats implements Serializable {
  public long numPeriods;
  public long numWords;

  public DocumentStats() {}

  public DocumentStats(long numPeriods, long numWords) {
    this.numPeriods = numPeriods;
    this.numWords = numWords;
  }
}
