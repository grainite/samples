package org.sample.wordcount;

import com.grainite.api.Cursor;
import com.grainite.api.Event;
import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.GrainiteException;
import com.grainite.api.Key;
import com.grainite.api.KeyValue;
import com.grainite.api.MapQuery;
import com.grainite.api.Table;
import com.grainite.api.Topic;
import com.grainite.api.Value;
import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Client {

  private void load(Grainite client, String inputFile) throws Exception {

    // STEP 1 BEGIN

    // TODO: Parse lines from inputFile and append them
    // as Events to line_topic.

    // STEP 1 END
  }

  private void getWordCount(Grainite client, String word)
      throws GrainiteException {

    // STEP 2 BEGIN

    // TODO: Get the count for 'word' from the Word Stats table.

    // STEP 2 END
  }

  private void getDocumentStats(Grainite client, String docName)
      throws Exception {

    // STEP 3 BEGIN

    // TODO: Get the document stats from the Doc Stats table.

    // STEP 3 END
  }

  private void getAllWordsStats(Grainite client) throws Exception {

    // STEP 4 BEGIN

    // TODO: Query all grains from the Word Stats table, and scan all the words
    // from each grain. Display the words along with their counts in sorted
    // order.

    // STEP 4 END
  }

  final static byte[] MAX_KEY = new byte[] {(byte)255, 0};

  private static void usage() {
    System.err.println("Usage: wordcount < cmd > [ params ]");
    System.err.println("where commands are:");
    System.err.println("     load: load <path to document>");
    System.err.println(
        "  genload: genload <num documents> <num sentences per doc>");
    System.err.println("     word: word <word>");
    System.err.println(" allwords: allwords");
    System.err.println("      doc: doc <document name>\n");
    System.exit(1);
  }

  public static void main(String[] args) throws Exception {
    if (args.length < 1) {
      // command and load are required to be passed.
      usage();
    }

    String host = "localhost";
    Grainite client = GrainiteClient.getClient(host, 5056);

    String cmd = args[0];

    switch (cmd.toLowerCase()) {
    case "load":
      new Client().load(client, args[1]);
      break;
    case "word":
      new Client().getWordCount(client, args[1]);
      break;
    case "doc":
      new Client().getDocumentStats(client, args[1]);
      break;
    case "allwords":
      new Client().getAllWordsStats(client);
      break;
    default:
      usage();
    }

    client.close();
  }
}