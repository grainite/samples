#!/bin/bash

if [ $# -lt 1 ] || [ $# -gt 2 ]; then
  echo ====== $#
  echo 'Usage: < cmd > [ args ]'
  exit 1
fi

echo java -cp target/wordcount-jar-with-dependencies.jar org.sample.wordcount.Client $@
java -cp target/wordcount-jar-with-dependencies.jar org.sample.wordcount.Client $@
