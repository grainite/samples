#!/bin/bash -e

# BASEDIR must point to the /samples repository in order to locate the TLS certificates, if required
BASEDIR=$(cd "$(dirname "$0")"; pwd)/..
source ${BASEDIR}/certs/_func

if [ $# -lt 3 ] || [ $# -gt 5 ]; then
  echo ====== $#
  java -cp ${BASEDIR}/wordcount/target/wordcount-0.1-SNAPSHOT-jar-with-dependencies.jar com.grainite.samples.wordcount.WordCount
  exit 1
fi

# Check if the required certifcates exist

CA_CERT="${BASEDIR}/certs/grainite_ca.crt"
CLIENT_CERT=$( get_client_crt )
CLIENT_KEY=$( get_client_key )
CLIENT_CERT=${CLIENT_CERT:-"client.crt"}
CLIENT_KEY=${CLIENT_KEY:-"client.key"}

echo java -cp ${BASEDIR}/wordcount/target/wordcount-0.1-SNAPSHOT-jar-with-dependencies.jar com.grainite.samples.wordcount.WordCount  $CA_CERT $CLIENT_CERT $CLIENT_KEY $@
java -cp ${BASEDIR}/wordcount/target/wordcount-0.1-SNAPSHOT-jar-with-dependencies.jar com.grainite.samples.wordcount.WordCount $CA_CERT $CLIENT_CERT $CLIENT_KEY $@

