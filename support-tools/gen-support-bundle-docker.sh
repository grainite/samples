#!/bin/bash

#set -eou pipefail
#set -x

PGCMD="${BASH_SOURCE:-${0}}"
PGDIR="$(cd ${PGCMD%/*};pwd)"
PGNAME="${PGCMD##*/}"

SUPPORT_TOOLS_DIR=$(cd "$(dirname "$0")"; pwd)
INTERNAL_TOOLS=(
    ${SUPPORT_TOOLS_DIR}/support_comm.rc.sh
    ${SUPPORT_TOOLS_DIR}/docker.rc.sh
    ${SUPPORT_TOOLS_DIR}/gen-support-bundle-internal.sh
)

usage() {
    cat << EOF
${0} [ -c container_name ] [ -d dx_dir_path ] [ -o ] [ -a ] [ -h ]
     -o: directory for output files
     -a: collect core files
     -h: help
EOF
    exit 1
}

cname=grainite
while getopts "ho:c:d:a" args; do
    case "${args}" in
        o) output_dir=${OPTARG};;
        c) cname=${OPTARG};;
        d) dx_path=${OPTARG};;
        a) detailed_opt="-a";;
        h|*) usage;;
    esac
done

if [[ -z "${output_dir}" ]]; then
    output_dir=$(mktemp -d /tmp/sub.XXXXXX)
else
    mkdir -p ${output_dir}
fi

exec &> >(tee  ${output_dir}/run.log)
docker exec ${cname}  ls > /dev/null 2>&1
if [[ $? -ne 0 ]]; then
    echo "Container ${cname} is not in healthy state. Collecting from outside of container ${cname}"

    if [[ ! -d ${dx_path} ]]; then
        echo "Missing dx directory"
        usage
    fi

    sudo ${SUPPORT_TOOLS_DIR}/gen-support-bundle-external.sh -d ${dx_path} -o ${output_dir} ${detailed_opt:-}
else
    echo "Collecting from inside of constainer ${cname}"
    TOOL_DIR=/tmp/support-tools
    docker exec ${cname} ls ${TOOL_DIR} > /dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        docker exec ${cname} mkdir -p ${TOOL_DIR}
    fi
    for tool_file in ${INTERNAL_TOOLS[@]}; do
        docker cp ${tool_file} ${cname}:${TOOL_DIR}
    done
    docker exec ${cname} ${TOOL_DIR}/gen-support-bundle-internal.sh -o ${output_dir} ${detailed_opt:-}
    docker exec ${cname} tar -C ${output_dir} -cf - --ignore-failed-read . 2> /dev/null | tar -xf - -C ${output_dir} 2> /dev/null 
fi
