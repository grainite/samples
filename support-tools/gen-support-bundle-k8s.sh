#!/bin/bash

#set -eou pipefail
#set -x

PGCMD="${BASH_SOURCE:-${0}}"
PGDIR="$(cd ${PGCMD%/*};pwd)"
PGNAME="${PGCMD##*/}"

SUPPORT_TOOLS_DIR=$(cd "$(dirname "$0")"; pwd)

source ${SUPPORT_TOOLS_DIR}/support_comm.rc.sh
source ${SUPPORT_TOOLS_DIR}/k8s.rc.sh

usage() {
    cat << EOF
${0} < -c cluster_name > [ -g gx binary path (optional) ] [ -o output directory ] [ -a ] [ -h ] [ -n namespace ]
     -a: Get detailed information including potentially large artifacts
     -h: help
EOF
    exit 1
}

namespace="default"
while getopts "c:n:o:g:a" args; do
    case "${args}" in
        c) cluster_name=${OPTARG};;
        n) namespace=${OPTARG};;
        o) output_dir=${OPTARG};;
        g) gxBin=${OPTARG};;
        a) detailedInfo=true;;
        h|*) usage;;
    esac
done

if [[ -z "${cluster_name}" ]]; then
    usage
fi

if [[ -z "${detailedInfo}" ]]; then
    detailedInfo=false
fi

if [[ -z "${gxBin}" ]]; then
    GX_OUTPUT=$(which gx)
    if [[ -z ${GX_OUTPUT} ]]; then
        echo "gx not found, provide gx binary path"
        exit 1
    fi
    gxBin="${GX_OUTPUT}"
fi


if [[ -z "${output_dir}" ]]; then
    output_dir=$(mktemp -d /tmp/sub.XXXXXX)
else
    mkdir -p ${output_dir}
fi

exec &> >(tee  ${output_dir}/run.log)
log INFO "Collection started @ $(date)"

check_package
context=$(get_context ${cluster_name})
KBCTL_CMD="kubectl --context ${context} -n ${namespace}"
TLS_ENABLED=$(${KBCTL_CMD} get cm grainite-config -ojsonpath={.data.enable_tls})
GX_CMD="${gxBin}"

LOG_DIR=$(${KBCTL_CMD} get cm grainite-config -ojsonpath={.data.grainite-log})
META_DIR=$(${KBCTL_CMD} get cm grainite-config -ojsonpath={.data.grainite-meta})
if [[ -z ${LOG_DIR} || -z ${META_DIR} ]]; then
    log ERR "Missing meta or log directory"
    exit 1
fi

gxcluster_dir=${output_dir}/gxcluster
mkdir -p ${gxcluster_dir}
log INFO "Collecting info for gxcluster"
get_gxcluster_info ${gxcluster_dir}

kbcluster_dir=${output_dir}/k8scluster/cluster_info
mkdir -p ${kbcluster_dir}
log INFO "Collecting info for kubernetes cluster"
get_kbcluster_info ${kbcluster_dir}

kbpod_dir=${output_dir}/k8scluster/pods
mkdir -p ${kbcluster_dir}

for p in $(${KBCTL_CMD} get pods -lcomponent=grainite -ojsonpath='{.items[*].metadata.name}'); do
    log INFO "  Collecting info for pod ${p}"
    mkdir -p ${kbpod_dir}/${p}
    get_pod_console_log ${p} ${kbpod_dir}/${p} grainite
    if [[ ${detailedInfo} ==  "true" ]]; then
        prep_core_files ${p}
    fi

    sidecar_vols=( $( ${KBCTL_CMD} get sts/gxs -ojsonpath='{.spec.template.spec.containers[?(@.name=="sidecar")].volumeMounts[*].name}') )
    if [[ ${#sidecar_vols[@]} -gt 0 ]]; then
        container_name="sidecar"
    else
        container_name="grainite"
    fi

    get_pod_log ${p} ${kbpod_dir}/${p} ${container_name} ${detailedInfo}
    get_pod_meta ${p} ${kbpod_dir}/${p} ${container_name}
    get_pod_state ${p} ${kbpod_dir}/${p} grainite
done

for p in $(${KBCTL_CMD} get pods -lcomponent=jetty -ojsonpath='{.items[*].metadata.name}'); do
    log INFO "  Collecting info for pod ${p}"
    mkdir -p ${kbpod_dir}/${p}
    get_pod_console_log ${p} ${kbpod_dir}/${p} jetty
    get_pod_log ${p} ${kbpod_dir}/${p} jetty
    get_pod_state ${p} ${kbpod_dir}/${p} jetty
done


log INFO "All states and logs are stored in ${output_dir}"
log INFO "Please run \"tar -C ${output_dir} -czvf /tmp/sub.tar.gz .\" after reviewing the contents in ${output_dir}"

log INFO "Collection completed @ $(date)"
