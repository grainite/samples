#!/bin/bash

# set -eou pipefail
# set -x

PGCMD="${BASH_SOURCE:-${0}}"
PGDIR="$(cd ${PGCMD%/*};pwd)"
PGNAME="${PGCMD##*/}"

SUPPORT_TOOLS_DIR=$(cd "$(dirname "$0")"; pwd)

source ${SUPPORT_TOOLS_DIR}/support_comm.rc.sh
source ${SUPPORT_TOOLS_DIR}/docker.rc.sh

usage() {
    cat << EOF
$0 < -d dx_dir > < -o  output_dir > [ -h ]
     -h: help
EOF
    exit 1
}

cname=grainite
while getopts "ho:c:d:a" args; do
    case "$args" in
        o) output_dir=${OPTARG};;
        c) cname=${OPTARG};;
        d) dx_path=${OPTARG};;
        a) detailed="true";;
        h|*) usage;;
    esac
done

if [[ -z "$dx_path" || -z "$output_dir" || ! -d "$output_dir" ]]; then
    usage
fi

exec &> >(tee  $output_dir/run.log)
log INFO "Collection started @ $(date)"

check_package
LOG_DIR=$dx_path
META_DIR=$dx_path

container_dir=$output_dir/container
mkdir -p $container_dir

log INFO "Collecting info for container"
get_container_log $container_dir
get_container_meta $container_dir
if [[ "${detailed:-}" == "true" ]]; then
    get_core_files ${container_dir} ${LOG_DIR}
fi

log INFO "All states and logs are stored in $output_dir"
log INFO "Please run \"tar -C $output_dir -czvf /tmp/sub.tar.gz .\" after reviewing the contents in $output_dir"

log INFO "Collection completed @ $(date)"
