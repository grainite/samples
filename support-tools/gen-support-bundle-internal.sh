#!/bin/bash

#set -eou pipefail
#set -x

PGCMD="${BASH_SOURCE:-${0}}"
PGDIR="$(cd ${PGCMD%/*};pwd)"
PGNAME="${PGCMD##*/}"

## All paths should be derived from GRAINITE_PKG as absolute paths
GRAINITE_HOME="${GRAINITE_HOME:-"${PGDIR%/*/*}"}"
GRAINITE_PKG="${GRAINITE_PKG:-"${GRAINITE_HOME}"}"
TOOL_DIR=/tmp/support-tools
source ${TOOL_DIR}/support_comm.rc.sh
source ${TOOL_DIR}/docker.rc.sh

export PATH=${PATH}:${GRAINITE_PKG}/grainite/bin

gx_bin=${GRAINITE_PKG}/grainite/bin/gx
dx_path=${GRAINITE_HOME}/dx


while getopts "ho:a" args; do
    case "$args" in
        o) output_dir=${OPTARG};;
        a) detailed="true";;
        h|*) usage;;
    esac
done

if [[ -z ${output_dir} ]]; then
    log ERR "Missing output_dir"
    exit 1
fi
mkdir ${output_dir}

exec &> >(tee  ${output_dir}/run.log)
log INFO "Collection started @ $(date)"

check_package
GX_CMD="${gx_bin}"
LOG_DIR=${dx_path}
META_DIR=${dx_path}

gxcluster_dir=${output_dir}/gxcluster
mkdir -p ${gxcluster_dir}
get_gxcluster_info ${gxcluster_dir}

container_dir=${output_dir}/container
mkdir -p ${container_dir}

log INFO "Collecting info for container"
get_container_log ${container_dir}
get_container_meta ${container_dir}
get_container_state ${container_dir}

if [[ "${detailed:-}" == "true" ]]; then
    get_core_files ${container_dir}
fi

log INFO "All states and logs are stored in ${output_dir}"
log INFO "Please run \"tar -C ${output_dir} -czvf /tmp/sub.tar.gz .\" after reviewing the contents in ${output_dir}"

log INFO "Collection completed @ $(date)"
