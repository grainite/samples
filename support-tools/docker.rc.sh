## All paths should be derived from GRAINITE_PKG as absolute paths

declare -a PACKAGE_NEEDED=( "awk" "curl" "tar" "tr" )

function get_container_log () {
    local output=${1}
    local log_output=${output}/logs

    log INFO "  Collecting all logs ${LOG_DIR}"

    mkdir -p ${log_output}
    local -a log_list=()
    local logfile
    for logfile in ${LOG_DIR}/*; do
        logfile=${logfile##*/}
        if [[ $logfile =~ "log"$ && $logfile != "topic_log" ]]; then
            log_list+=( $logfile )
        fi
    done
    log DEBUG "    Collecting log file: ${log_list[@]}"
    tar -C ${LOG_DIR} -cf - ${log_list[@]} | tar xf - -C ${log_output}
}

function get_container_meta () {
    local output=${1}
    local meta_dir=${output}/meta

    mkdir -p ${meta_dir}

    log INFO "  Collecting info for ${META_DIR}"
    tar -C ${META_DIR} -cf - --exclude '*.war' --exclude '*.jar'  ${meta_file[@]} 2> /dev/null | tar xf - -C ${meta_dir} 2> /dev/null
}

function get_container_state () {
    local output=${1}
    local state_dir=${output}/container-state

    mkdir -p ${state_dir}

    log INFO "  Collecting current state"
    date > ${state_dir}/date.txt
    ps aux > ${state_dir}/ps.txt
    lsblk > ${state_dir}/blk.txt
    df -h > ${state_dir}/df.txt
    ls -lR /home/grainite /dev > ${state_dir}/ls.txt
    netstat -tunap > ${state_dir}/netstat.txt
    gx_node_info localhost 0 ${state_dir}
}

function get_cluster_health () {
    local output=${1}
    local health_port=5064
    local cdir=${META_DIR}/certs

    log INFO "  Collecting gxcluster health"
    curl -m 3 localhost:${health_port}/health > ${output}/health 2> /dev/null
}

function get_meta_data () {
    local output=${1}

    SYSCONFIG_CMD="${GRAINITE_PKG}/grainite/corebin/config_sysconfig.py"

    for k in ${md_keys[@]}; do
        ${SYSCONFIG_CMD} config --op 0 --key "${k}" >> ${output}/meta-data 2>/dev/null
        if [[ $? -ne 0 ]]; then
            next_pod=true
            break
        fi
    done
}

function get_storage_info () {
    local output=${1}

    log INFO "  Collecting gxcluster storage info"
    CONFIG_CMD="${GRAINITE_PKG}/grainite/corebin/config_client.py"
    ${CONFIG_CMD} get-storage-stats >> ${output}/storage-info 2>/dev/null
}

function get_core_files () {
    local output=${1}
    local core_dir=${2:-"/home/grainite/dx"}/cores

    log INFO "  Collecting cores if any"
    mkdir -p ${output}/cores
    for cf in $( ls ${core_dir} 2> /dev/null ); do
        corefile=$(basename ${cf})
        if [[ ${corefile} =~ ".gz"$ ]]; then
            log INFO "    Collecting ${corefile}"
            cp ${core_dir}/${corefile} ${output}/cores
            continue
        fi
        log INFO "    Compressing ${corefile}"
        gzip -c ${core_dir}/${corefile} > ${output}/cores/${corefile}.gz
    done

    if [[ ! -z  ${corefile} && ${core_dir} == "/home/grainite/dx/cores" ]];then
        if [[ ! -s ${core_dir}/gxsrvr.gz ]]; then
            log INFO "    Compressing gxsrvr"
            gzip -c ${GRAINITE_PKG}/grainite/corebin/gxsrvr > ${core_dir}/gxsrvr.gz
        fi

        if [[ ! -s ${core_dir}/gxsmxr.gz ]]; then
            log INFO "    Compressing gxsmxr"
            gzip -c ${GRAINITE_PKG}/grainite/corebin/gxsmxr > ${core_dir}/gxsmxr.gz
        fi
        cp ${core_dir}/gxsrvr.gz ${core_dir}/gxsmxr.gz ${output}/cores/
    fi
}

function get_gxcluster_info () {
    local output=${1}

    log INFO "Collecting info for gxcluster"
    gx_cluster_info localhost ${output}
    get_app_info localhost ${output}
    #gx_storage_info $(get_cluster_ip) ${output}
    get_cluster_health ${output}
    get_meta_data ${output}
    get_storage_info ${output}
}
