## All paths should be derived from GRAINITE_PKG as absolute paths for internal scripts

function array_join {
    local IFS="$1"; shift; echo "$*";
}

## with set -e [pipefail], this will not exit with error if no match
## should be used with command substitution only, i.e var=$(cmd | grepnorr [|cmd])
function grepnoerr () {
    grep $@ || test $? = 1
}

## with set -e [pipefail], this will not exit with error if no match
## should be used with command substitution only, i.e var=$(cmd | grepnorr [|cmd])
function pgrepnoerr () {
    grep $@ || test $? = 1
}

function log () {
    local level="${1}";shift
    local ts=$(date "+%Y-%m-%d %H:%M:%S ${level}")
    local context="[${PGNAME}] === "
    echo -e "\n"${ts} "${context}" "$@"
}

function check_package () {
    for p in ${PACKAGE_NEEDED}; do
        which ${p} > /dev/null 2>&1
        if [[ $? -ne 0 ]]; then
            log ERR "Missing ${p} in ${PATH}"
        fi
    done
}

##### gx commands #####
function gx_node_info () {
    local host=${1}
    local node=${2}
    local output=${3}

    ${GX_CMD} node info --skip-health-check ${node} -H ${host} > ${output}/gx-node-info
}

function gx_cluster_info () {
    local host=${1}
    local output=${2}

    log INFO "  Collecting gxcluster config"
    ${GX_CMD} cluster info --skip-health-check -H ${host} > ${output}/cluster-info
}

function gx_storage_info () {
    local host=${1}
    local output=${2}

    log INFO "  Collecting info for storage"
    ${GX_CMD} cluster stats storage --skip-health-check -H ${host} > ${output}/storage-info
}

declare -a meta_file=(
    "app_base_dir"
    "app_path"
    "jetty-base"
    "pod_lifecycle"
    "ns.conf"
)

##### gxcluster status #####
declare -a md_keys=(
    "/cluster_config"
    "/system_config/__BOOT_MARKER__"
    "/system_config/app_config"
    "/system_config/meta_data/app_control"
    "/system_config/meta_data/app_info"
    "/system_config/meta_data/cluster"
    "/system_config/meta_data/replog"
    "/system_config/meta_data/v2/segment"
)

GRAINITE_HOME="/home/grainite"
function get_app_info () {
    local host=${1}
    local output=${2}/app_info
    local apps app

    log INFO "  Collecting info for app"
    mkdir -p ${output}
    apps=( $( ${GX_CMD} app ls --skip-health-check -H ${host} |grep ^[0-9]*\\. |awk '{print $2}' ) )
    for app in ${apps[@]}; do
        log INFO "    Collecting info for ${app}"
        local env_app
        env_app=( $( echo ${app} | tr ":" " ") )
        ${GX_CMD} app info --skip-health-check -e ${env_app[0]} -a ${env_app[1]} -H ${host} > ${output}/${app}
    done
}
