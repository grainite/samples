## All paths should be derived from GRAINITE_PKG as absolute paths

declare -a PACKAGE_NEEDED=( "awk" "kubectl" "curl" "tar" "tr" )

##### K8s commands #####
function get_context () {
    local cluster_name=${1}

    ## ignore -e pipefail
    context=$(kubectl config current-context |grepnoerr "${cluster_name}$")
    if [ ! -n "${context}" ]; then
        clusters=( $(kubectl config get-clusters |grepnoerr "${cluster_name}$" |awk '{print $1}') )
        if [[ ${#clusters[@]} -ne 1 ]]; then
            log ERR "Multiple clusters { ${clusters[@]} } matching ${cluster_name}"
            exit 1
        fi
        context=$( kubectl config get-contexts | grepnoerr "${clusters[0]}" | awk '{print $1}')
    fi
    echo ${context}
}

function get_cluster_ip () {
    host_name=$( ${KBCTL_CMD} get svc gslb -ojsonpath={'.status.loadBalancer.ingress[0].hostname'} )
    host_ip=$( ${KBCTL_CMD} get svc gslb -ojsonpath={'.status.loadBalancer.ingress[0].ip'} )
    if [[ -n "${host_name}" ]]; then
        echo ${host_name}
        return
    fi
    if [[ -n "${host_ip}" ]]; then
        echo ${host_ip}
    fi
}

function get_running_pods () {
    ${KBCTL_CMD} get pods -o jsonpath='{range .items[*]}{.status.containerStatuses[*].running.true}{.metadata.name}{ "\n"}{end}'
}

function get_resource_info () {
    local res=${1}

    log INFO "  Collecting info for ${res}"
    ${KBCTL_CMD} get ${res} -owide > ${output}/${res}.txt
    ${KBCTL_CMD} get ${res} -oyaml > ${output}/${res}.yaml
    ${KBCTL_CMD} describe ${res} > ${output}/${res}.desc
}

function get_kbcluster_info () {
    local output=${1}

    log INFO "  Collecting info for kube-system namespace"
    ${KBCTL_CMD} get all -n kube-system > ${output}/kube-system.txt
    ${KBCTL_CMD} get all -n kube-system -oyaml > ${output}/kube-system.yaml
    ${KBCTL_CMD} describe all -n kube-system  > ${output}/kube-system.desc

    log INFO "  Collecting configmap grainite-config"
    ${KBCTL_CMD} get cm grainite-config -oyaml > ${output}/config-map.yaml
    get_resource_info nodes
    get_resource_info pods
    get_resource_info pv
    get_resource_info pvc
    get_resource_info sc
    get_resource_info svc
    get_resource_info ep

    log INFO "  Collecting events"
    ${KBCTL_CMD} get events > ${output}/events.txt

}
##### Pod specific info #####
function get_pod_console_log () {
    local pod_name=${1}
    local output=${2}
    local container_name=${3}

    log INFO "    Collecting console log for ${pod_name}"
    ${KBCTL_CMD} -c ${container_name} logs ${pod_name}> ${output}/console.log
}

function get_core_dir () {
    local pod_name=${1}

    local core_patt=$( ${KBCTL_CMD} exec -c grainite ${pod_name} -- bash -c "cat /proc/sys/kernel/core_pattern")
    local core_dir=${core_patt%core*}

    if [[ "${core_dir}" == "" ]]; then
        echo ${HOME}/pkg
    else
        echo ${core_dir}
    fi
}

function prep_core_files () {
    local pod_name=${1}

    log INFO "    Preparing cores file for ${pod_name}"
    core_dir=$( get_core_dir  ${pod_name} )
    ${KBCTL_CMD} exec -c grainite ${pod_name} -- bash -c \
        'LOGDIR=$(cat /home/grainite/config/grainite-log); \
        mkdir -p ${LOGDIR}/cores;             \
        mv ${core_dir}/core.* ${LOGDIR}/cores > /dev/null 2>&1;  \
        ls ${LOGDIR}/cores/core.* > /dev/null 2>&1; \
        if [[ $? -eq 0 ]]; then \
            if [[ ! -s ${LOGDIR}/cores/gxsrvr.gz ]]; then \
                cp ${GRAINITE_PKG}/grainite/corebin/gxsrvr ${GRAINITE_PKG}/grainite/corebin/gxsmxr ${LOGDIR}/cores; \
            fi; \
            ${GRAINITE_PKG}/grainite/corebin/zip_cores.sh; \
        fi'
}

function get_pod_log () {
    local pod_name=${1}
    local output=${2}
    local container_name=${3}
    local detailedInfo=${4}
    local log_output=${output}/logs
    local log_dir=${LOG_DIR}

    log INFO "    Collecting all logs in ${pod_name}"

    mkdir -p ${log_output}

    if [[ ${container_name} =~ jetty ]]; then
        log_dir=$META_DIR/remote-jetty/jetty-log/$pod_name
    fi
    if [[ ${detailedInfo} ==  "true" ]]; then
        ${KBCTL_CMD} -c ${container_name} cp ${pod_name}:${log_dir} ${log_output} > /dev/null
    else
        ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- tar -C ${log_dir} -cf - --exclude 'cores' . 2> /dev/null | tar xf - -C ${log_output} 2> /dev/null
    fi
}

function get_pod_meta () {
    local pod_name=${1}
    local output=${2}
    local meta_dir=${output}/meta
    local container_name=${3}

    mkdir -p ${meta_dir}

    log INFO "    Collecting info for ${META_DIR} in ${pod_name}"

    if [[ ${pod_name} == "gxs-0" ]]; then
        local remote_jetty_base="remote-jetty/jetty-base"
    fi

    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- tar -C ${META_DIR} -cf - --exclude '*.war' --exclude '*.jar'  ${meta_file[@]} ${remote_jetty_base} 2> /dev/null | tar xf - -C ${meta_dir} 2> /dev/null
}

function get_pod_state () {
    local pod_name=${1}
    local output=${2}
    local container_name=${3}
    local state_dir=${output}/pod-state

    mkdir -p ${state_dir}

    log INFO "    Collecting current state in ${pod_name}"
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 date > ${state_dir}/date.txt
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 ps aux > ${state_dir}/ps.txt
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 lsblk > ${state_dir}/blk.txt
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 df -h > ${state_dir}/df.txt
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 ls -lR /grainite '/home/grainite' /dev > ${state_dir}/ls.txt
    ${KBCTL_CMD} exec -c ${container_name} ${pod_name} -- env COLUMNS=256 netstat -tunap > ${state_dir}/netstat.txt
    if [[ ${container_name} =~ grainite ]]; then
        gx_node_info $(get_cluster_ip) ${pod_name##gxs-} ${state_dir}
    fi
}

##### gxcluster status #####
function get_cluster_health () {
    local output=${1}
    local health_port=5064
    local cdir=${META_DIR}/certs
    local pods pod
    pods=( $( get_running_pods ) )

    log INFO "  Collecting gxcluster health"
    ${KBCTL_CMD} exec -c grainite ${pods[@]} -- ls ${cdir} > /dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        CURL_CMD="curl --cacert ${cdir}/grainite_ca.crt --cert ${cdir}/grainite_server.crt --key ${cdir}/grainite_server.key -m 3 https://localhost:${health_port}/health"
    else
        CURL_CMD="curl -m 3 localhost:${health_port}/health"
    fi

    for pod in ${pods[@]}; do
        echo "Using ${pod} to collect health info" > ${output}/health
        ${KBCTL_CMD} exec -c grainite ${pod} -- ${CURL_CMD} >> ${output}/health 2> /dev/null
        if [[ $? -eq 0 ]]; then
            return
        fi
    done
}

function get_meta_data () {
    local output=${1}
    local cdir=${META_DIR}/certs
    local pods=( $( get_running_pods ) )
    local pod next_pod

    ${KBCTL_CMD} exec -c grainite ${pods[@]} -- ls ${cdir} > /dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        SYSCONFIG_CMD="${GRAINITE_HOME}/grainite/corebin/config_sysconfig.py --enable_tls --certs_dir=${cdir}"
    else
        SYSCONFIG_CMD="${GRAINITE_HOME}/grainite/corebin/config_sysconfig.py"
    fi

    log INFO "  Collecting gxcluster meta_data"
    for pod in ${pods[@]}; do
        next_pod=false
        echo "Using pod ${pod} for collection" > ${output}/meta-data
        for k in ${md_keys[@]}; do
            ${KBCTL_CMD} exec -c grainite ${pod} -- ${SYSCONFIG_CMD} config --op 0 --key "${k}" >> ${output}/meta-data 2>/dev/null
            if [[ $? -ne 0 ]]; then
                next_pod=true
                break
            fi
        done
        if [[ ${next_pod} == false ]]; then
            break
        fi
    done

}

function get_storage_info () {
    local output=${1}
    local cdir=${META_DIR}/certs
    local pods=( $( get_running_pods ) )

    ${KBCTL_CMD} exec -c grainite ${pods[@]} -- ls ${cdir} > /dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        CONFIG_CMD="${GRAINITE_HOME}/grainite/corebin/config_client.py --enable_tls --certs_dir=${cdir}"
    else
        CONFIG_CMD="${GRAINITE_HOME}/grainite/corebin/config_client.py"
    fi
    log INFO "  Collecting gxcluster storage info"
    for pod in ${pods[@]}; do
        echo "Using ${pod} to collect storage info" > ${output}/storage-info
        ${KBCTL_CMD} exec -c grainite ${pod} -- ${CONFIG_CMD} get-storage-stats >> ${output}/storage-info 2>/dev/null
        if [[ $? -eq 0 ]]; then
            break
        fi
    done
}

function get_gxcluster_info () {
    local output=${1}

    gx_cluster_info $(get_cluster_ip) ${output}
    get_app_info $(get_cluster_ip) ${output}
    get_cluster_health ${output}
    get_meta_data ${output}
    get_storage_info ${output}
}
