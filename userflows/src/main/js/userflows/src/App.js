import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import { Card, CardBody, CardHeader, CardTitle, CardSubtitle, Col, Container, Row, Table } from 'reactstrap';

function App() {

  const [response, setResponse] = useState({})
  const [userInfo, setUserInfo] = useState()
  const [userGuid, setUserGuid] = useState()

  const refreshUsersTable = () => {
    let data = [];

    data[0] = [];
    data[1] = [];
    data[2] = [];
    data[3] = [];
    data[4] = [];
    data[5] = [];
    
    console.log("The response is: " + response);
    console.log("The number of keys: " + Object.keys(response).length);
    Object.keys(response).forEach(function(key, i) {
      console.log("i = " + i + " key = " + key);
      console.log("response[key] " + response[key]);
      console.log("The remainder is " + Math.floor(i/10));
      data[Math.floor(i/10)].push(<tr onClick={() => getUserInfo(key)}>
        <td>{key}</td>
        <td>{response[key]}</td>
      </tr>);
    });


    return (
      <Container>
        <Row>
          <Col> <Table striped bordered hover> <thead> <tr> <th>ID #</th> <th>Active Flows</th> </tr> </thead> <tbody> {data[0]} </tbody> </Table> </Col>
          <Col> <Table striped bordered hover> <thead> <tr> <th>ID #</th> <th>Active Flows</th> </tr> </thead> <tbody> {data[1]} </tbody> </Table> </Col>
          <Col> <Table striped bordered hover> <thead> <tr> <th>ID #</th> <th>Active Flows</th> </tr> </thead> <tbody> {data[2]} </tbody> </Table> </Col>
          <Col> <Table striped bordered hover> <thead> <tr> <th>ID #</th> <th>Active Flows</th> </tr> </thead> <tbody> {data[3]} </tbody> </Table> </Col>
          <Col> <Table striped bordered hover> <thead> <tr> <th>ID #</th> <th>Active Flows</th> </tr> </thead> <tbody> {data[4]} </tbody> </Table> </Col>
        </Row>
      </Container>
    );
  }

  const showUserDetails = (userInfo) => {
    if (userInfo === null || userInfo === undefined || !('flowState' in userInfo))
      return;

    let flows = [];

    flows.push(<thead><tr><th>Flow Name</th><th>Current Step in Flow</th><th># actions in Flow</th></tr></thead>)

    Object.keys(userInfo.flowState).forEach(function(key) {
      flows.push(<tr><td>{key}</td><td>{userInfo.flowState[key][0].location}</td><td>{userInfo.flowState[key].length}</td></tr>);
    });

    return (<Card><CardHeader className="bg-primary">User {userGuid}</CardHeader><CardBody><Table bordered>{flows}</Table></CardBody></Card>);
  }

  const getUserInfo = (guid) => {
    console.log("The user info is " + guid)
    setUserGuid(guid)
    fetch('http://localhost:8081/userdetails?guid=' + guid)
    .then(res => res.json())
      .then((data) => {
        console.log(data);
        setUserInfo(data);
      })
      .catch(console.log);
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      fetch('http://localhost:8081/useractivity')
      .then(res => res.json())
      .then((data) => {
        //console.log(data);
        setResponse(data);
        if (userGuid !== undefined && userGuid !== null) {
          getUserInfo(userGuid);
        }
      })
      .catch(console.log);
    }, 1000);
    return () => clearTimeout(timer);
  });

  return (
    <Container>
      {refreshUsersTable()}
      {showUserDetails(userInfo)}
    </Container>
  );
}

export default App;
