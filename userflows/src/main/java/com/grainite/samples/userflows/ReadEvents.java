package com.grainite.samples.userflows;

import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.Topic;
import com.grainite.api.TopicReadCursor;

public class ReadEvents {
  static int numRecords = 0;

  public static void main(String[] args) {
    Grainite g = GrainiteClient.getClient(args[0], 5056);
    Topic t = g.getTopic(Constants.APP_NAME, Constants.UPDATES_TOPIC);

    TopicReadCursor cursor = t.newReadCursor(args[1], null, 1, 1, true);

    while (cursor.hasNext()) {
      cursor.next();
      numRecords++;
      cursor.save();
    }
    System.out.println("Total Records: " + numRecords);
  }
}
