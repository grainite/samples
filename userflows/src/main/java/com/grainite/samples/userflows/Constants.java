package com.grainite.samples.userflows;

public interface Constants {
  public final static String APP_NAME = "userflows";
  public final static String UPDATES_TOPIC = "updates_topic";
  public final static String USER_TABLE = "user_table";
  public final static String FLOW_STATS_TABLE = "flow_stats_table";
  public final static String USER_STATS_TABLE = "user_stats_table";
  public static final String FLOW_STATS_ACTION = "flowStats";
  public static final String USER_STATS_ACTION = "userStats";
  public static final String USER_GRAIN_UPDATES_ACTION = "updateEvents";
}
