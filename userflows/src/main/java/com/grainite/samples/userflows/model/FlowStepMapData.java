package com.grainite.samples.userflows.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FlowStepMapData {
  public static Map<String, ArrayList<String>> flowStepMap;
  public static ArrayList<String> channels;

  static {
    flowStepMap = new HashMap<>();
    ArrayList<String> replaceCardSteps = new ArrayList<String>() {
      private static final long serialVersionUID = 1L;

      {
        add("authenticate");
        add("selectcard");
        add("comments");
        add("confirm");
        add("complete");
      }
    };

    flowStepMap.put("replaceCreditCard", replaceCardSteps);

    ArrayList<String> redeemPointsMagazineSubscriptionSteps = new ArrayList<String>() {
      private static final long serialVersionUID = 1L;

      {
        add("authorize");
        add("selectMagazine");
        add("selectPeriod");
        add("confirm");
        add("complete");
      }
    };

    flowStepMap.put("redeemPointsMagazineSubscription", redeemPointsMagazineSubscriptionSteps);

    ArrayList<String> increaseSpendingLimitSteps = new ArrayList<String>() {
      private static final long serialVersionUID = 1L;

      {
        add("authenticate");
        add("doCreditCheck");
        add("viewOptions");
        add("selectLimit");
        add("confirm");
        add("complete");
      }
    };
    flowStepMap.put("increaseSpendingLimit", increaseSpendingLimitSteps);

    ArrayList<String> redeemPointsBookHotelSteps = new ArrayList<String>() {
      private static final long serialVersionUID = 1L;

      {
        add("authorize");
        add("selectLocation");
        add("selectHotel");
        add("chooseRoom");
        add("confirm");
        add("complete");
      }
    };

    flowStepMap.put("redeemPointsBookHotel", redeemPointsBookHotelSteps);

    channels = new ArrayList<String>() {
      private static final long serialVersionUID = 1L;

      {
        add("desktop");
        add("mobile");
        add("phone");
      }
    };
  }
}
