package com.grainite.samples.userflows;

import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.QueryExpression;
import com.grainite.api.QueryExpression.SimpleQueryExpression;
import com.grainite.api.Table;
import com.grainite.api.TypedValue;
import java.util.Iterator;
import java.util.concurrent.CountDownLatch;

public class FindGrains {
  static int numRecords = 0;
  static CountDownLatch latch = null;

  public static void main(String[] args) throws Exception {
    if (args.length < 2) {
      System.out.println(
          "Using default values for arguments flowName & location : replaceCreditCard, authenticate");
      args = new String[] {"replaceCreditCard", "authenticate"};
    }

    Grainite g = GrainiteClient.getClient("localhost", 5056);

    // Query user table for indexed entries matching
    // key:location-"flowName" and value: "location"
    Table t = g.getTable(Constants.APP_NAME, Constants.USER_TABLE);
    QueryExpression queryExpression =
        QueryExpression.newBuilder()
            .setQueryExpression(
                SimpleQueryExpression.newBuilder().setParameters(
                    TypedValue.of("location-" + args[0]),
                    TypedValue.of(args[1])))
            .build();

    Iterator<Grain> it = t.find(queryExpression);

    while (it.hasNext()) {
      numRecords += 1;
      it.next();
    }

    System.out.println("Total Records: " + numRecords);
  }
}
