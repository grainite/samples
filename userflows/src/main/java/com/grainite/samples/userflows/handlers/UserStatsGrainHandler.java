package com.grainite.samples.userflows.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.GrainRequest;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import com.grainite.samples.userflows.model.FlowStatistics;

/**
 * The UserStats grain receives messages from the UserGrain, and keeps track of the top
 * 50 user by amount of activity. A client may fetch this data, and then query the
 * UserGrain to get details about these top 50 users.
 *
 * The UserStats grain maintains a priority queue, prioritized on the number of steps that
 * a user might be performing.
 */

public class UserStatsGrainHandler {
  public ActionResult handleUserStats(Action action, GrainContext context) throws IOException {
    UserActivityMeta meta = context.getValue().asType(UserActivityMeta.class);

    if (!(action instanceof GrainRequest)) {
      return ActionResult.failure(action, "Invalid action type");
    }

    GrainRequest request = (GrainRequest) action;

    FlowStatistics event = request.getPayload().asType(FlowStatistics.class);
    meta.addEntry(event.publicGuid, event.numSteps);
    context.setValue(Value.ofObject(meta));

    return ActionResult.success(action, Value.ofObject(meta));
  }

  /**
   * Keeps a max of 50 (TOP_N) users in a PriorityQueue organized by # of steps that the
   * user has performed.
   */

  public static class UserActivityMeta implements Serializable {
    private static final long serialVersionUID = 1L;
    private Map<String, UserSteps> users = new HashMap<>();
    private PriorityQueue<UserSteps> stepsQueue = new PriorityQueue<>(new StepComparator());
    private final static int TOP_N = 50;

    @Override
    public String toString() {
      return stepsQueue.toString();
    }

    public PriorityQueue<UserSteps> getStepsQueue() {
      return stepsQueue;
    }

    public void addEntry(String userId, int numSteps) {
      UserSteps steps = users.get(userId);
      if (steps != null) {
        // if the user's entry is already in the top50, simply update it.
        stepsQueue.remove(steps);
        steps.stepCount = numSteps;
        stepsQueue.add(steps);
        return;
      }

      steps = new UserSteps(userId, numSteps);

      if (users.size() < TOP_N) {
        // if the size of the map is smaller than 50, add the new entry
        stepsQueue.add(steps);
        users.put(userId, steps);
      } else {
        // otherwise, we check if the new steps is more than the least, if so, we remove
        // the least and add
        // this instead.
        UserSteps least = stepsQueue.peek();
        if (least.stepCount < numSteps) {
          users.remove(least.userId);
          stepsQueue.offer(steps);
          users.put(userId, steps);
        }
      }
    }

    public static class UserSteps implements Serializable {
      private static final long serialVersionUID = 1L;
      public String userId;
      public int stepCount;

      public UserSteps(String userId, int stepCount) {
        this.userId = userId;
        this.stepCount = stepCount;
      }
    }

    static class StepComparator implements Comparator<UserSteps>, Serializable {
      private static final long serialVersionUID = 1L;

      // note: reverse sort, least steps at the top of the queue
      @Override
      public int compare(UserSteps first, UserSteps second) {
        return Integer.compare(second.stepCount, first.stepCount);
      }
    }
  }
}
