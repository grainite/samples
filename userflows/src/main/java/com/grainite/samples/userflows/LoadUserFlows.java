package com.grainite.samples.userflows;

import com.grainite.api.Callback;
import com.grainite.api.Event;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.GrainiteException;
import com.grainite.api.RecordMetadata;
import com.grainite.api.Topic;
import com.grainite.api.TopicDedupOptions;
import com.grainite.api.Value;
import com.grainite.samples.loadgen.Throttler;
import com.jsoniter.any.Any;
import com.jsoniter.output.JsonStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class LoadUserFlows {
  static String dataDir;
  static String envName;
  static int waitTime = 0;
  static String extraPayload;
  static Random rand = new Random(1357);

  static int numPublicGuids = 10000;
  private static Map<Integer, UserInfo> userFlows = new HashMap<>();

  static class UserInfo {
    Map<String, Integer> currentFlows = new HashMap<>();
    Map<String, String> sessionInfo = new HashMap<>();
  }

  private static String[] channels = {"desktop", "mobile", "phone"};
  private static String[] flowNames = {
      "replaceCreditCard", "redeemPointsMagazineSubscription",
      "increaseSpendingLimit", "redeemPointsBookHotel"};
  private static String[][] flowSteps = {
      {"authenticate", "selectcard", "comments", "confirm", "complete"},
      {"authorize", "selectMagazine", "selectPeriod", "confirm", "complete"},
      {"authenticate", "doCreditCheck", "viewOptions", "selectLimit", "confirm",
       "complete"},
      {"authorize", "selectLocation", "selectHotel", "chooseRoom", "confirm",
       "complete"},
  };

  public static void main(String[] args) throws IOException, GrainiteException {
    if (args.length < 1 || args.length > 8) {
      System.err.printf(
          "Usage: < host IP > < number of events > < target TPS > [ env name ] [ true/false - enable topic dedup ] < path to ca cert > <path to client cert > < path to client key >\n");
      System.exit(1);
    }
    String host = args[0];
    int port = 5056;
    int numEvents = -1;  // unlimited
    int targetTPS = 100; // default tps
    boolean enableTopicDedup = false;

    File caCert = null;
    File clientCert = null;
    File clientKey = null;

    if (args.length >= 2) {
      numEvents = Integer.parseInt(args[1]);
    }
    if (args.length >= 3) {
      targetTPS = Integer.parseInt(args[2]);
    }
    if (args.length >= 4) {
      envName = args[3];
    }
    if (args.length >= 5) {
      enableTopicDedup = Boolean.parseBoolean(args[4]);
    }
    if (args.length >= 6) {
      caCert = Paths.get(args[5]).toFile();
      clientCert = Paths.get(args[6]).toFile();
      clientKey = Paths.get(args[7]).toFile();
    }

    System.out.println("host:" + host + " port:" + port +
                       " numEvents:" + numEvents + " tps:" + targetTPS);

    Grainite client = null;

    if (caCert != null && clientCert != null && clientKey != null) {
      // TLS client.
      client = GrainiteClient.getClient(
          host, port, caCert.getAbsolutePath(), clientCert.getAbsolutePath(),
          clientKey.getAbsolutePath(), envName == null ? "default" : envName);
    } else {
      client = GrainiteClient.getClient(host, port,
                                        envName == null ? "default" : envName);
    }

    if (!generateEvents(client, numEvents, targetTPS, enableTopicDedup)) {
      System.out.println("event generation failed");
      System.exit(1);
    }
  }

  public static boolean generateEvents(Grainite client, int numEvents,
                                       int targetTPS,
                                       boolean enableTopicDedup) {
    /*
    StringBuilder sb = new StringBuilder(32000);
    for (int i=0; i<16000; i++) {
      sb.append('a' + rand.nextInt(26) % 3);
    }
    extraPayload = sb.toString();
    */
    extraPayload = "abc";
    long startTime = System.currentTimeMillis();
    Topic topic = client.getTopic(
        Constants.APP_NAME, Constants.UPDATES_TOPIC,
        TopicDedupOptions.newBuilder().setDedupStatus(enableTopicDedup));
    Throttler throttler = new Throttler(targetTPS);

    System.out.println("Topic dedup status: " + enableTopicDedup);

    AtomicLong success = new AtomicLong(0);

    int numEventsDone = 0;

    // Produce events
    while (numEvents < 0 ? true : numEventsDone < numEvents) {
      Any event = getNextEvent();
      String guid = event.toString("requestPayload", "user", "publicGuid");
      topic.appendAsync(
          new Event(Value.of(guid),
                    Value.of(JsonStream.serialize(event).getBytes())),
          new Callback<RecordMetadata>() {
            @Override
            public void onCompletion(RecordMetadata completion,
                                     Exception exception) {
              if (exception != null) {
                long ms = System.currentTimeMillis() - startTime;
                System.out.printf(
                    "\n%8d events successfully appended in %5d ms at %5.1f TPS",
                    success.get(), ms, ((float)success.get() * 1000 / ms));
                System.err.printf("Exception: %s\nPlease try again..",
                                  exception.toString());
                System.exit(1);
              } else
                success.incrementAndGet();
            }
          });

      numEventsDone++;
      throttler.throttle(1);
    }

    topic.flush();

    while (success.get() < numEvents) {
      long ms = System.currentTimeMillis() - startTime;
      System.out.printf(
          "\n%8d events completed, %5d ms, %5.1f TPS, %8d succeeded\n",
          numEventsDone, ms, ((float)numEventsDone * 1000 / ms), success.get());
      try {
        Thread.sleep(100);
      } catch (InterruptedException e) {
        // Ignore.
      }
    }

    long ms = System.currentTimeMillis() - startTime;
    System.out.printf(
        "\n%8d events completed, %5d ms, %5.1f TPS, %8d succeeded\n",
        numEventsDone, ms, ((float)numEventsDone * 1000 / ms), success.get());

    System.out.println();

    client.close();

    return success.get() == numEvents;
  }

  public static Any getNextEvent() {
    int publicGuid = rand.nextInt(numPublicGuids);
    int curStep = 0;
    int flowNum = rand.nextInt(flowNames.length);
    String flow = flowNames[flowNum];

    UserInfo userInfo = userFlows.get(publicGuid);
    if (userInfo == null) {
      userFlows.put(publicGuid, userInfo = new UserInfo());
      userInfo.currentFlows.put(flowNames[rand.nextInt(flowNames.length)], 0);
      userInfo.sessionInfo.put(channels[rand.nextInt(channels.length)],
                               String.format("%d", System.currentTimeMillis()));
    } else {
      if (userInfo.currentFlows.containsKey(flow)) {
        int prevStep = userInfo.currentFlows.get(flow);
        int curStepProb = rand.nextInt(10) + 1; // between 1 and 10
        if (curStepProb <= 8) {
          curStep = prevStep + 1;
          if (flowSteps[flowNum].length <= curStep) {
            // start the flow over again.
            curStep = 0;
            userInfo.currentFlows.put(flow, curStep);
            // userInfo.currentFlows.remove(flow);
          } else {
            userInfo.currentFlows.put(flow, curStep);
          }
        } else if (curStepProb == 10) {
          // cancel flow
          curStep = -1;
          userInfo.currentFlows.remove(flow);
        } else {
          if (prevStep >= 1) {
            curStep = prevStep - 1; // move back a step
            userInfo.currentFlows.put(flow, curStep);
          }
        }
      } else {
        curStep = 0;
        userInfo.currentFlows.put(flow, 0);
      }
    }

    int sameSessionProb = rand.nextInt(10) + 1; // 1 to 10
    int sessionInfoKeys = userInfo.sessionInfo.keySet().size();

    String[] sessionKeys = userInfo.sessionInfo.keySet().toArray(new String[0]);
    String[] sessionVals = userInfo.sessionInfo.values().toArray(new String[0]);
    String visitorId = String.format("%d", System.currentTimeMillis());
    int channelProb = rand.nextInt(3); // between 0 and 2
    String channel = channels[channelProb];

    if ((sessionInfoKeys == 1 && sameSessionProb <= 8) ||
        (sessionInfoKeys == 2 && sameSessionProb <= 4) ||
        (sessionInfoKeys == 3 && sameSessionProb <= 3)) {
      channel = sessionKeys[0];
      visitorId = sessionVals[0];
    } else if ((sessionInfoKeys == 2 && sameSessionProb > 4 &&
                sameSessionProb <= 8) ||
               (sessionInfoKeys == 3 && sameSessionProb >= 4 &&
                sameSessionProb <= 7)) {
      channel = sessionKeys[1];
      visitorId = sessionVals[1];
    } else if (sessionInfoKeys == 3) {
      visitorId = sessionVals[2];
      channel = sessionKeys[2];
    }

    userInfo.sessionInfo.put(channel, visitorId);

    // return Json object

    Map<String, Map<String, String>> requestPayload = new HashMap<>();
    requestPayload.put("user", new HashMap<>());
    requestPayload.put("view", new HashMap<>());
    requestPayload.put("device", new HashMap<>());

    requestPayload.get("user").put("publicGuid", "" + publicGuid);
    requestPayload.get("user").put("visitorId", visitorId);
    requestPayload.get("view").put(
        "location", (curStep == -1) ? "abandon" : flowSteps[flowNum][curStep]);

    requestPayload.get("view").put("flow", flow);
    requestPayload.get("device").put("channel", channel);
    requestPayload.get("user").put("extraworkload", extraPayload);

    Map<String, Map<String, Map<String, String>>> event = new HashMap<>();
    event.put("requestPayload", requestPayload);

    return Any.wrap(event);
  }
}
