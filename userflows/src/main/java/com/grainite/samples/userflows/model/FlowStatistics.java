package com.grainite.samples.userflows.model;

import java.io.Serializable;

public class FlowStatistics implements Serializable {
  private static final long serialVersionUID = 1L;

  public FlowStatistics() {}

  public FlowStatistics(String currStep, String channel, String flowName, String publicGuid,
      String visitorId, int numSteps) {
    this.location = currStep;
    this.channel = channel;
    this.flowName = flowName;
    this.publicGuid = publicGuid;
    this.visitorId = visitorId;
    this.numSteps = numSteps;
  }

  public String publicGuid;
  public String visitorId;
  public String location;
  public String prevLocation;
  public String channel;
  public String flowName;
  public int numSteps;
  public long ts;
}