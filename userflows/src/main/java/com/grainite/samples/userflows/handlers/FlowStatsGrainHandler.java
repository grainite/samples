package com.grainite.samples.userflows.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.GrainRequest;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.grainite.samples.userflows.model.FlowStatistics;

/**
 * FlowStats maintains statistics per flow across all users. Each FlowStats grain manages the state
 * for a particular flow.
 */
public class FlowStatsGrainHandler {
  /**
   * Handles messages arriving from the UserGrain to record statistics for each flow.
   */
  public ActionResult handleFlowStats(Action action, GrainContext context) throws IOException {
    FlowMeta meta = context.getValue().asType(FlowMeta.class);

    if (!(action instanceof GrainRequest)) {
      return ActionResult.failure(action, "Invalid action type");
    }

    GrainRequest grainRequest = (GrainRequest) action;

    FlowStatistics event = grainRequest.getPayload().asType(FlowStatistics.class);

    // Update the metadata with the total count of the steps of the flow for the channel
    meta.flowName = event.flowName;
    meta.countEvent(event);
    context.setValue(Value.ofObject(meta));

    return ActionResult.success(action, context.getValue());
  }

  public static class FlowMeta implements Serializable {
    private static final long serialVersionUID = 6060654805224056339L;

    public String flowName;
    // flow step / location -> flowstepinfo
    public Map<String, FlowStepInfo> flowSteps = new HashMap<>();

    public void countEvent(FlowStatistics flowStats) {
      FlowStepInfo stepInfo =
          flowSteps.computeIfAbsent(flowStats.location, k -> new FlowStepInfo());
      stepInfo.channelCount.compute(flowStats.channel, (k, v) -> (v == null) ? 1 : v + 1);
      if (flowStats.prevLocation != null && "abandon".equals(flowStats.location)) {
        flowSteps.compute(flowStats.prevLocation,
            (k, v)
                -> (v == null) ? new FlowStepInfo().incrementAbandonCount()
                               : v.incrementAbandonCount());
      }
    }

    public String toString() {
      return flowSteps.toString();
    }

    // For each step, tracks count of users on a channel, as well as number of users
    // that might have abandoned at this step.
    public static class FlowStepInfo implements Serializable {
      private static final long serialVersionUID = -9108889610050285615L;
      public Map<String, Integer> channelCount = new HashMap<>();
      public int abandonCounts = 0;

      public String toString() {
        return channelCount.toString();
      }

      public FlowStepInfo incrementAbandonCount() {
        abandonCounts++;
        return this;
      }
    }
  }
}
