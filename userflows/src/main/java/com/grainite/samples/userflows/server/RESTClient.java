package com.grainite.samples.userflows.server;

import com.grainite.api.Grain;
import com.grainite.api.Grainite;
import com.grainite.api.GrainiteClient;
import com.grainite.api.Table;
import com.grainite.api.Value;
import com.grainite.samples.userflows.Constants;
import com.grainite.samples.userflows.handlers.UserGrainUpdatesHandler;
import com.grainite.samples.userflows.handlers.UserGrainUpdatesHandler.UserMeta;
import com.grainite.samples.userflows.handlers.UserStatsGrainHandler;
import com.grainite.samples.userflows.handlers.UserStatsGrainHandler.UserActivityMeta;
import com.grainite.samples.userflows.handlers.UserStatsGrainHandler.UserActivityMeta.UserSteps;
import com.jsoniter.output.JsonStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
@EnableScheduling
public class RESTClient {
  HashMap<String, Integer> users = new HashMap<>();
  public String host = "localhost";
  /**
   *
   */
  @Scheduled(fixedRate = 1000)
  public void getRideStats() {
    try {
      Grainite client = GrainiteClient.getClient(host, 5056);
      Table userStatsTable =
          client.getTable(Constants.APP_NAME, Constants.USER_STATS_TABLE);
      Grain userStats = userStatsTable.getGrain(Value.of(1));
      UserActivityMeta meta = userStats.getValue().asType(
          UserStatsGrainHandler.UserActivityMeta.class);
      PriorityQueue<UserSteps> userSteps = meta.getStepsQueue();
      Iterator<UserSteps> userStepIter = userSteps.iterator();
      HashMap<String, Integer> currUsers = new HashMap<>();
      int count = 0;
      while (userStepIter.hasNext() && (count <= 50)) {
        UserSteps userStep = userStepIter.next();
        String userId = userStep.userId;
        int stepCount = userStep.stepCount;
        currUsers.put(userId, stepCount);
        count++;
      }
      users = currUsers;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(value = "/useractivity")
  public String getUserActivity() {
    return JsonStream.serialize(users);
  }

  @CrossOrigin(origins = "*")
  @RequestMapping(value = "/userdetails", method = RequestMethod.GET)
  public String getUserInfo(@RequestParam("guid") String guid) {
    try {
      Grainite client = GrainiteClient.getClient(host, 5056);
      Table userStatsTable =
          client.getTable(Constants.APP_NAME, Constants.USER_TABLE);
      Grain userGrain = userStatsTable.getGrain(Value.of(guid));
      UserMeta userMeta =
          userGrain.getValue().asType(UserGrainUpdatesHandler.UserMeta.class);
      String userMetaStr = JsonStream.serialize(userMeta);
      System.out.println("tHe user meta obj is: " + userMetaStr);
      return userMetaStr;
    } catch (Exception e) {
      e.printStackTrace();
      return "";
    }
  }
}
