package com.grainite.samples.userflows.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.GrainContext;

public class IllegalArgumentExceptionHandler {
  public ActionResult handleIllegalArgumentException(Action action, Throwable t,
                                                     GrainContext context) {
    if (!(t instanceof IllegalArgumentException)) {
      return ActionResult.failure(action, "Unexpected exception type.");
    } else {
      return ActionResult.success(action,
                                  Value.of("Expected runtime exception."));
    }
  }
}
