package com.grainite.samples.userflows.handlers;

import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.TopicEvent;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.grainite.api.TypedValue;
import com.grainite.api.Key;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.grainite.samples.userflows.Constants;
import com.grainite.samples.userflows.model.FlowStatistics;

/**
 * The UserGrain is subscribed to the Updates topic and receives TopicEvent actions as events are
 * appended to the Updates topic. <p> It aggregates the information about each update into it's
 * local grain state stored as the UserMeta class.
 */
public class UserGrainUpdatesHandler {
  private static final String ABANDONED_FLOWS = "abandoned_flows";
  private static final String COMPLETED_FLOWS = "completed_flows";
  private static final String ACTIVE_USER_GRAINS = "active_user_grains";

  public ActionResult handleUpdateEvents(Action action, GrainContext context) throws IOException {
    UserMeta meta = context.getValue().asType(UserMeta.class);

    context.counter(ACTIVE_USER_GRAINS).inc();

    if (!(action instanceof TopicEvent)) {
      return ActionResult.failure(action, "Invalid action type");
    }

    context.counter("num_events_processed").inc();

    TopicEvent topicRequest = (TopicEvent) action;
    Any event = JsonIterator.deserialize(topicRequest.getPayload().asBytes(), Any.class);

    String guid = event.toString("requestPayload", "user", "publicGuid");
    String visitorId = event.toString("requestPayload", "user", "visitorId");
    String channel = event.toString("requestPayload", "device", "channel");
    String location = event.toString("requestPayload", "view", "location");
    String flowName = event.toString("requestPayload", "view", "flow");

    FlowStatistics flowStats =
        new FlowStatistics(location, channel, flowName, guid, visitorId, meta.numFlowSteps + 1);
    long currentTime = System.currentTimeMillis();

    if ("complete".equals(location) || "abandon".equals(location)) {
      // remove the flow from the state
      context.getLogger().info("==== COMPLETE or ABANDON");
      flowStats.prevLocation = meta.addFlowStep(
          flowName, new UserMeta.FlowStep(location, channel, currentTime, visitorId));
      meta.removeFlow(flowName);
    } else {
      flowStats.prevLocation = meta.addFlowStep(
          flowName, new UserMeta.FlowStep(location, channel, currentTime, visitorId));
    }

    if (flowStats.prevLocation != null) {
      context
          .counter("current_flow_counts_done", CounterParam.of("flow_name", flowStats.flowName),
              CounterParam.of("location", flowStats.prevLocation))
          .inc();
      context.updateIndexEntry(Key.of("location-" + flowName),
          TypedValue.of(flowStats.prevLocation), TypedValue.of(location));
    } else {
      context
          .counter("current_flow_counts", CounterParam.of("flow_name", flowStats.flowName),
              CounterParam.of("location", flowStats.location))
          .inc();
      context.insertIndexEntry(Key.of("location-" + flowName), TypedValue.of(location));
    }

    if ("complete".equals(location)) {
      context.counter(COMPLETED_FLOWS, CounterParam.of("flow_name", flowStats.flowName)).inc();
    } else if ("abandon".equals(location)) {
      context.counter(ABANDONED_FLOWS, CounterParam.of("flow_name", flowStats.flowName)).inc();
    }

    // send statistics to the global and user activity stats namespaces.

    // Send message to the Flow Stats Grain, which is organized by flowname
    context.sendToGrain(Constants.FLOW_STATS_TABLE, Value.of(flowName),
        new GrainContext.GrainOp.Invoke(Constants.FLOW_STATS_ACTION, Value.ofObject(flowStats)),
        null);

    // Also send to User Stats grain, which has only one entry - identified by a single key - 1
    context.sendToGrain(Constants.USER_STATS_TABLE, Value.of(1),
        new GrainContext.GrainOp.Invoke(Constants.USER_STATS_ACTION, Value.ofObject(flowStats)),
        null);

    context.setValue(Value.ofObject(meta));

    context.counter(ACTIVE_USER_GRAINS + "_done").inc();

    return ActionResult.success(action, Value.ofObject(meta));
  }

  /**
   * UserMeta stores all information about the flows and the flowsteps for a user.
   */
  public static class UserMeta implements Serializable {
    private static final long serialVersionUID = 1484937734458550368L;

    Map<String, List<FlowStep>> flowState = new HashMap<>();
    int numFlowSteps = 0; // number of flow steps the user has transitioned between

    // add a flow step, return the prev step
    public String addFlowStep(String flowName, FlowStep step) {
      String previousStep = null;
      if (flowState.containsKey(flowName)) {
        previousStep = flowState.get(flowName).get(0).location;
        flowState.get(flowName).add(0, step);
      } else {
        flowState.put(flowName, new ArrayList<>(Arrays.asList(step)));
      }
      numFlowSteps++;
      return previousStep;
    }

    public void removeFlow(String flowName) {
      flowState.remove(flowName);
    }

    @Override
    public String toString() {
      return "UserMeta{"
          + "flowState=" + flowState + ", numFlowSteps=" + numFlowSteps + '}';
    }

    public static class FlowStep implements Serializable {
      private static final long serialVersionUID = 1L;

      String location;
      String channel;
      long timestamp;
      String visitorId;

      public FlowStep() {}

      public FlowStep(String loc, String chan, long ts, String visId) {
        location = loc;
        channel = chan;
        timestamp = ts;
        visitorId = visId;
      }
    }
  }
}
