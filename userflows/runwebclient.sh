#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")"; pwd)/..

(cd ${BASEDIR}; ./mvnw -pl userflows -am clean spring-boot:run)
