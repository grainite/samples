#!/bin/bash -e
##

BASEDIR=$(cd "$(dirname "$0")"; pwd)/..
source ${BASEDIR}/certs/_func

if [ $# -lt 3 ] || [ $# -gt 6 ]; then
  echo ====== $#
  echo 'Usage: < host IP > < number of events > < target TPS > [ env name ] [ true/false - enable topic dedup ]'
  exit 1
fi

# Check if the required certifcates exist

CA_CERT="${BASEDIR}/certs/grainite_ca.crt"
CLIENT_CERT=$( get_client_crt )
CLIENT_KEY=$( get_client_key )
ENV="default"
DEDUP="false"

# Set environment
if [ "$#" -ge 4 ]; then
  ENV="$4"
fi

# Set dedup
if [ "$#" -ge 5 ]; then
  DEDUP="$5"
fi

if ! [[ -f "${CA_CERT}" && -f "${CLIENT_CERT}" && -f "${CLIENT_KEY}" ]]; then
  # Connect without TLS. 
  echo [WARNING] Certificates were not found under ${BASEDIR}/certs. Running without TLS.
  unset CA_CERT
  unset CLIENT_CERT
  unset CLIENT_KEY
fi 

echo java -cp ${BASEDIR}/userflows/target/userflows-0.1-SNAPSHOT-jar-with-dependencies.jar com.grainite.samples.userflows.LoadUserFlows ${@: 1:3} ${ENV} ${DEDUP} ${CA_CERT} ${CLIENT_CERT} ${CLIENT_KEY}
java -cp ${BASEDIR}/userflows/target/userflows-0.1-SNAPSHOT-jar-with-dependencies.jar com.grainite.samples.userflows.LoadUserFlows ${@: 1:3} ${ENV} ${DEDUP} ${CA_CERT} ${CLIENT_CERT} ${CLIENT_KEY}
