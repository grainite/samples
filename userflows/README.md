# Userflows

**Userflows** shows how Grainite can be used to analyze user session (cookie) data in real-time and provide the latest state of the user sessions to applications that end-users interact with so that users get a seamless experience even when switching between devices.


![Userflows Application](../docs/resources/userflows.gif "Userflows Application")

<br>

Note: If you haven't already, please follow [**the quickstart guide**](../quickstart.md) to set up the gx CLI and compile the sample apps, inclduing Userflows.

## Deploy Userflows onto Grainite

`gx` reads the application YAML file to parse what tables and topics are needed, and where the app jar is located. This information is then sent to the Grainite server to deploy the application.

```bash
gx load -H <server host>
```

Note: By default, gx will look for a YAML configuration file in the current directory. If one is not found or is not valid, gx will throw an exception for most commands. To specify a particular YAML configuration file, you can use **`gx <sub command(s)> -c <path to config file>`** .

## Run Userflows

Run a script to send **1000 events** at a rate of **10 events/second** to the Grainite server.

```bash
./load.sh <server host> 1000 10
```

## Observing Userflows

To see what happened, you can look at a couple of things:

1. App log
2. Events in a Topic
3. Grains in a Table

### 1. App Log

The following will print the application logs.

```bash
gx log --follow
```

### 2. Events in a Topic

Run the below command. `gx` will then ask you to select a topic in the application and will print the events in that topic.

```bash
gx topic dump
```

### 3. Grains in a Table

Run the below command. `gx` will then ask you to select a table in the application and will print the grains in that table.

```bash
gx table dump
```

<br><br>


# Advanced: Monitoring Userflows using Prometheus and Grafana

Grainite makes it easy for developers to track metrics using Prometheus by providing access to counters and gauges in the Action Handlers themselves. Userflows utilizes these APIs to track various metrics for users as well as flows using Prometheus and display them on a Grafana dashboard.

### Prometheus Setup

Follow the guide [**here**](https://prometheus.io/docs/prometheus/latest/installation/) to install Prometheus.

The app directory contains a `prometheus.yml` file under `monitoring/`, which should be used for your Prometheus instance.

```bash
global:
  scrape_interval:     15s # Set the scrape interval to every 15 seconds. Default is every 1 minute.
  evaluation_interval: 15s # Evaluate rules every 15 seconds. The default is every 1 minute.
  # scrape_timeout is set to the global default (10s).

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: 'userflows_grainite_prometheus'
    metrics_path: '/export-app'
    scrape_interval: 5s

    static_configs:
    - targets: ['localhost:5154']
```

Note: If you are **not running Grainite locally in a Docker container**, be sure to **change** the **target in `prometheus.yml`** to point to the correct host.

### **Grafana Setup**

Follow the guide [**here**](https://grafana.com/docs/grafana/latest/installation/) to install Grafana.

Add the Prometheus data source to Grafana. Be sure to change the URL to point to your Prometheus installation.

![Prometheus Data Source in Grafana](../docs/resources/prometheus.png "Prometheus Data Source in Grafana")

Finally, import the userflows dashboard, found at `monitoring/UserFlowsDashboard.json` into Grafana to view the metrics.

![Grafana Dashboard](../docs/resources/grafana.png "Grafana Dashboard")

<br><br>


# Advanced: Userflows Web Application (In Unsecure/TLS disabled Docker)

In addition to the Grafana dashboard, there is also a React app packaged in Userflows which shows a real-time view of users and their progression through various flows.

Before we start the React application, we need to start a Spring Boot server that queries Grainite and provides an API that can be accessed by the React application. We can do this running **`./runwebclient.sh`** which starts the Spring Boot server on port **8081.**

Note: If you are **not running Grainite locally in a Docker container**, be sure to **change** the **host in `src/main/java/com/grainite/samples/userflows/server/RESTClient.java`** to point to the correct host and recompile userflows.

Now we can start the react application by heading over to **`src/main/js/userflows`** and running the following:

```bash
npm install
npm start
```

This will start the react application on port **4400** which can then be viewed on \`[http://localhost:4400/](http://localhost:4400)\`.

![Userflows Web Application](../docs/resources/userflows-web-app.png "Userflows Web Application")

