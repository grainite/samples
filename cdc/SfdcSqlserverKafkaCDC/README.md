# SQL Server and Salesforce CDC to Kafka

This application shows how Grainite can stream changes from Salesforce and SQL Server to Kafka in real-time. In this example, the data events are directly transferred to Kafka without any transformations. 

## A few points to note:
Change events from SQL Server are pulled by Grainite tasks using the CDC mechanism in SQL Server. 
Events from Salesforce can be pulled either using SOQL queries or the CDC mechanism if available. This is specified in the configuration in the application YAML file. 
Since no transformations are performed in this example, there is no code to write in Grainite. The application YAML file contains all the configurations required to enable this streaming. 
Grainite maintains the cursors to the data sources and is responsible for sending events to Kafka in a reliable, exactly-once manner. 

Additionally, Grainite embeds a database engine that is optimized for streaming. This enables powerful capabilities for transforming the data streams in a stateful manner. The transforms, filters, and streaming joins can be done in Python or Java. 

The application is available here. Before running it, your grainite environment should be set up with version 2326.11 or above. Follow the steps here to get Grainite running on your VM or desktop. 
 
Get the connection properties of salesforce, sql server and kafka because they are required. For SQL Server, ensure that CDC is enabled. For Salesforce, if you do not have a license for CDC then set the useCdc property to false. 

## Diagram
![Architecture Diagram](sqlserversfdctokafka.jpg "Architecture Diagram")

## Steps
Set the properties of the salesforce and sql server reader tasks and Kafka Writer in app.yaml. 
Grainite provides a way to load sensitive information like passwords and private keys through the secrets mechanism. 
Then load the application into Grainite
```bash
gx load -c app.yaml
```


Load the secrets file for config params into Grainite. You can store the config params in a secrets file that you can upload to Grainite. These can be IDs and passwords as well as private key file used to connect to Salesforce. This way you do not need to put passwords in the clear in the app.yaml file 
```bash
sh secrets.sh
```

Start the salesforce task to pull data from Salesforce
```bash
gx task start sfdc_cdc
```

Start the SQL Server task to pull data from SQL Server
```bash
gx task start sqlserver_cdc
```

Grainite connects to Salesforce and SQL Server and starts pulling CDC events as soon as the tasks start. 
Run gx mon to monitor the status of these tasks and check how they are progressing. 
```bash
gx mon -c ap.yaml
```

Data will show up in the Kafka topic as these events are pulled from the sources and pushed to Kafka. 
