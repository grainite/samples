#!/bin/bash

# SQL script file to be executed
SQL_SCRIPT="db_setup.sql"


# Docker container name
ONPREM_CONTAINER_NAME="sql_onprem"
CLOUD_CONTAINER_NAME="sql_cloud"

echo "Setting onprem database"
# Setup on-prem
docker cp "$SQL_SCRIPT" "$ONPREM_CONTAINER_NAME:/tmp/$SQL_SCRIPT"
docker exec -it "$ONPREM_CONTAINER_NAME" /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Password123 -i "/tmp/$SQL_SCRIPT"

echo "Setting up cloud database"
# Setup cloud
docker cp "$SQL_SCRIPT" "$CLOUD_CONTAINER_NAME:/tmp/$SQL_SCRIPT"
docker exec -it "$CLOUD_CONTAINER_NAME" /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Password123 -i "/tmp/$SQL_SCRIPT"