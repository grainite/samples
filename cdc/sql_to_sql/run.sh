#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")"; pwd)

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 ./run.sh <dbConnectionURL> <insert_count>"
  echo "  eg: $0 ./run.sh jdbc:sqlserver://sql_onprem_ip:1433;databaseName=tempdb;encrypt=true;trustServerCertificate=true 10"
  exit 1
fi


java -cp target/sql_to_sql-jar-with-dependencies.jar org.samples.sql_to_sql.Client $@
