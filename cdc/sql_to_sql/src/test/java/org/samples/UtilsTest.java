package org.samples;

import static org.junit.Assert.assertEquals;

import com.jsoniter.any.Any;
import java.util.Collections;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.samples.sql_to_sql.Utils;

@RunWith(JUnit4.class)
public class UtilsTest {

  @Test
  public void testChangeMapDiff() {
    Map<String, Any> before, after, expected;
    // base case
    before = null;
    after = null;
    assertEquals(after, Utils.getChangeDiff(after, before));

    // new insert case
    before = null;
    after = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Bar"));
    assertEquals(after, Utils.getChangeDiff(after, before));

    // after is null
    before = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Bar"));
    after = null;
    assertEquals(null, Utils.getChangeDiff(after, before));

    // name property updated.
    before = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Foo"));
    after = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Bar"));
    expected = Map.of("Name", Any.wrap("Bar"));

    assertEquals(expected, Utils.getChangeDiff(after, before));

    // new property added to after.
    before = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Foo"));
    after = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Bar"), "Count", Any.wrap(1));
    expected = Map.of("Name", Any.wrap("Bar"), "Count", Any.wrap(1));
    assertEquals(expected, Utils.getChangeDiff(after, before));

    // property removed from before.
    before = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Foo"), "Count", Any.wrap(1));
    after = Map.of("Id", Any.wrap(1), "Name", Any.wrap("Foo"));
    assertEquals(Collections.EMPTY_MAP, Utils.getChangeDiff(after, before));
  }
}
