package org.samples.sql_to_sql;

import com.grainite.api.context.GrainContext;
import com.grainite.api.context.PoolAllocator;
import com.jsoniter.ValueType;
import com.jsoniter.any.Any;
import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCWriter {

  private static final int SOURCE_POOL_SIZE = 10;
  private static final int ERROR_CODE_PK_VIOLATION = 2627;

  private static SimpleDateFormat timeFormats[] =
      new SimpleDateFormat[] {new SimpleDateFormat("HH:mm:ss"), new SimpleDateFormat("HH:mm:ss.SSSSSS"),
                              new SimpleDateFormat("HH:mm:ss.SSSSSSZ")};

  private static SimpleDateFormat dateFormats[] =
      new SimpleDateFormat[] {new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"), new SimpleDateFormat("yyyy-MM-dd")};

  public static void write(GrainContext context, String dbURL, String tableName, String updateType, Any data,
                           String[] keyColumns) {

    int sourcePoolNum = Math.abs(keyColumns.toString().hashCode() % SOURCE_POOL_SIZE);
    String db_user = context.getConfig().get(Constants.SQL_CONNECTION_USERNAME);
    String db_password = context.getConfig().get(Constants.SQL_CONNECTION_PASSWORD);
    Connection connection = context.getPooledObject(dbURL + sourcePoolNum, new PoolAllocator() {
      @Override
      public Object allocate() {
        Connection conn = null;
        try {
          conn = DriverManager.getConnection(dbURL, db_user, db_password);
        } catch (SQLException e) {
          context.getLogger().info("Error in SQL connection");
          e.printStackTrace();
        }

        return conn;
      }

      @Override
      public void deallocate(Object object) {
        try {
          ((Connection)object).close();
        } catch (SQLException e) {
          context.getLogger().info("Error in closing SQL connection");
          e.printStackTrace();
        }
      }
    }, Connection.class);

    if (connection == null) {
      throw new IllegalStateException("Error in database connection");
    }

    Map<String, Integer> columns = new HashMap<>();
    try {
      ResultSet cols = connection.getMetaData().getColumns(null, null, tableName, null);
      while (cols.next()) {
        columns.put(cols.getString("COLUMN_NAME").toLowerCase(), Integer.parseInt(cols.getString("DATA_TYPE")));
      }
      executeSql(updateType, tableName, keyColumns, data, columns, connection, context, false);
    } catch (Exception e) {
      e.printStackTrace();
      throw new IllegalArgumentException(e.getMessage());
    }
  }

  private static int executeSql(String updateType, String table, String[] keyColumns, Any record,
                                Map<String, Integer> columnTypes, Connection c, GrainContext context,
                                boolean failOnUnknownFields) {
    String sql = null;

    boolean isCreate = updateType.equals("CREATE");
    Any[] recordIds = new Any[keyColumns.length];
    int[] recordIdTypes = new int[recordIds.length];

    StringBuffer keySql = new StringBuffer();

    if (!isCreate) {
      boolean skipRecord = false;
      for (int i = 0; i < keyColumns.length; i++) {
        if (!record.asMap().containsKey(keyColumns[i])) {
          context.getLogger().info("Skipping, could not locate key " + keyColumns[i] +
                                   " in record: " + record.toString());
          skipRecord = true;
        } else {
          recordIds[i] = record.get(keyColumns[i]);
          recordIdTypes[i] = columnTypes.get(keyColumns[i].toLowerCase());
          if (keySql.length() > 0) {
            keySql.append("AND ");
          }
          keySql.append(String.format("%s = ?", keyColumns[i]));
        }
      }

      if (skipRecord) {
        return 1;
      }
    }

    List<Any> parameters = new ArrayList<>();
    List<Integer> types = new ArrayList<>();

    if (updateType.equals("DELETE")) {
      sql = String.format("DELETE FROM %s WHERE %s", table, keySql.toString());
      for (int i = 0; i < recordIds.length; i++) {
        parameters.add(recordIds[i]);
        types.add(recordIdTypes[i]);
      }
    } else if (updateType.equals("UPDATE")) {
      StringBuffer updates = new StringBuffer();
      for (Object o : record.keys()) {
        if (isValidColumn(o.toString(), record.get(o), columnTypes, failOnUnknownFields)) {
          if (updates.length() > 0)
            updates.append(", ");
          updates.append(String.format("%s = ?", o.toString()));
          parameters.add(record.get(o));
          types.add(columnTypes.get(o.toString().toLowerCase()));
        }
      }
      sql = String.format("UPDATE %s SET %s WHERE %s", table, updates.toString(), keySql.toString());
      for (int i = 0; i < recordIds.length; i++) {
        parameters.add(recordIds[i]);
        types.add(recordIdTypes[i]);
      }
    } else if (updateType.equals("CREATE")) {
      StringBuffer columns = new StringBuffer(), values = new StringBuffer();

      for (Object o : record.keys()) {
        if (isValidColumn(o.toString(), record.get(o), columnTypes, failOnUnknownFields)) {
          if (columns.length() > 0) {
            columns.append(", ");
            values.append(", ");
          }

          columns.append(o.toString());
          values.append("?");
          parameters.add(record.get(o.toString()));
          types.add(columnTypes.get(o.toString().toLowerCase()));

          if (columnTypes.get(o.toString().toLowerCase()) == 93) {
            Any val = record.get(o.toString());
            System.out.println(val.toString());
          }
        }
      }

      sql = String.format("INSERT INTO %s (%s) VALUES (%s)", table, columns, values);
    } else {
      throw new IllegalArgumentException("Invalid change type: " + updateType);
    }

    if (parameters.size() == 0) {
      context.getLogger().info("No columns to update, skipping record: " + record.toString());
      return 1;
    }

    try {
      PreparedStatement ps = c.prepareStatement(sql);

      if (types.size() != parameters.size()) {
        // we have a problem
        context.getLogger().info("parameters and types don't match: " + Any.wrap(parameters).toString() + " " +
                                 Any.wrap(types).toString());
      }

      for (int i = 0; i < types.size(); i++) {
        int pType = types.get(i);
        Any pVal = parameters.get(i);

        if (!setParameter(ps, i + 1, pType, pVal)) {
          context.getLogger().info("Could not parse parameter for type: " + pType + " with val: " + pVal.toString() +
                                   ", ignoring");
        }
      }

      int mRows = ps.executeUpdate();
      return mRows;
    } catch (SQLException e) {
      if (e.getErrorCode() == ERROR_CODE_PK_VIOLATION) {
        context.getLogger().warning("Primary key violation, reattempting with UPDATE statement..");
        return executeSql("UPDATE", table, keyColumns, record, columnTypes, c, context, failOnUnknownFields);
      }
      e.printStackTrace();
      throw new IllegalStateException("Exception: " + e.getMessage() + " with " + sql, e);
    }
  }

  private static boolean setParameter(PreparedStatement ps, int pNum, int pType, Any pVal) throws SQLException {
    switch (pType) {
    case Types.BOOLEAN:
    case Types.BIT:
      ps.setBoolean(pNum, Boolean.parseBoolean(pVal.toString()));
      break;

    case Types.INTEGER:
    case Types.SMALLINT:
    case Types.TINYINT:
      ps.setInt(pNum, Integer.parseInt(pVal.toString()));
      break;

    case Types.DATE:
      boolean isParsed = false;
      for (int i = 0; i < dateFormats.length; i++) {
        try {
          ps.setDate(pNum, new Date(dateFormats[i].parse(pVal.toString()).getTime()));
          isParsed = true;
          break;
        } catch (Exception ex) {
        }
      }

      if (!isParsed) {
        ps.setNull(pNum, pType);
        return false;
      }
      break;

    case Types.DECIMAL:
    case Types.DOUBLE:
    case Types.REAL:
    case Types.NUMERIC:
      ps.setDouble(pNum, Double.parseDouble(pVal.toString()));
      break;

    case Types.FLOAT:
      ps.setFloat(pNum, Float.parseFloat(pVal.toString()));
      break;

    case Types.CHAR:
    case Types.LONGNVARCHAR:
    case Types.LONGVARCHAR:
    case Types.NCHAR:
    case Types.NVARCHAR:
    case Types.VARCHAR:
      ps.setString(pNum, pVal.toString());
      break;

    case Types.BIGINT:
      ps.setLong(pNum, Long.parseLong(pVal.toString()));
      break;

    case Types.NULL:
      ps.setNull(pNum, pType);
      break;

    case Types.TIMESTAMP:
    case Types.TIMESTAMP_WITH_TIMEZONE:
      if (pVal.valueType() == ValueType.NUMBER) {
        ps.setTimestamp(pNum, toTimestamp(pVal.toLong()));
      } else {
        Timestamp t = null;
        for (int i = 0; i < dateFormats.length; i++) {
          try {
            t = new Timestamp(dateFormats[i].parse(pVal.toString()).getTime());
            break;
          } catch (Exception ex) {
          }
        }

        if (t == null) {
          ps.setNull(pNum, pType);
          return false;
        } else {
          ps.setTimestamp(pNum, t);
        }
      }
      break;

    case Types.TIME:
    case Types.TIME_WITH_TIMEZONE:
      Instant instant = null;
      for (int i = 0; i < timeFormats.length; i++) {
        try {
          instant = timeFormats[i].parse(pVal.toString()).toInstant();
          break;
        } catch (Exception ex) {
        }
      }

      if (instant == null) {
        ps.setTime(pNum, Time.valueOf(pVal.toString()));
      } else {
        ps.setTime(pNum, new Time(instant.toEpochMilli()));
      }
      break;

    case Types.BINARY:
      byte[] blobBytes = pVal.toString().getBytes();
      ps.setBinaryStream(pNum, new ByteArrayInputStream(blobBytes), blobBytes.length);
      break;

    case Types.OTHER:
      ps.setObject(pNum, pVal.toString(), Types.OTHER);
      break;

    default:
      ps.setNull(pNum, pType);
      break;
    }

    return true;
  }

  private static Timestamp toTimestamp(long val) {
    if (val > 9999999999999999L) {
      // nanos.
      Timestamp t = new Timestamp(val / 1000000L);
      // t.setNanos((int) (val % 1000000L));
      return t;
    } else if (val > 9999999999999L) {
      // micros
      Timestamp t = new Timestamp(val / 1000L);
      // t.setNanos((int) ((val % 1000L) * 1000));
      return t;
    } else {
      // millis
      return new Timestamp(val);
    }
  }

  private static boolean isValidColumn(String name, Any val, Map<String, Integer> columnTypes, boolean failOnUnknown) {
    if (!columnTypes.containsKey(name.toLowerCase())) {
      if (failOnUnknown) {
        throw new IllegalArgumentException("Unknown field " + name + " in record");
      }
      return false;
    }

    int pType = columnTypes.get(name.toLowerCase());
    if (pType == Types.TIMESTAMP && val.valueType() == ValueType.NULL) {
      return false;
    }

    return true;
  }
}
