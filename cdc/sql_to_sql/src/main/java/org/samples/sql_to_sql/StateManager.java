package org.samples.sql_to_sql;

import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import com.jsoniter.output.JsonStream;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class StateManager {

  private static final Key TABLE_STATE_KEY = Key.of("TABLE_STATE");

  private GrainContext context;
  private RowState rowState;

  public StateManager(GrainContext context) {
    this.context = context;
    this.rowState = context.mapKeyExists(0, TABLE_STATE_KEY)
                        ? JsonIterator.deserialize(context.mapGet(0, TABLE_STATE_KEY).asString()).as(RowState.class)
                        : new RowState();
  }

  public ColumnState getColumnState(String tableName, String columnName, Map<String, Any> keysMap) {
    String stateID = buildStateId(tableName, columnName, keysMap);
    return rowState.getColumnState(stateID);
  }

  public void insert(String tableName, String columnName, Map<String, Any> keysMap, long version, String colValue,
                     Map<String, Boolean> isEchoPending) {
    String stateID = buildStateId(tableName, columnName, keysMap);
    ColumnState colState = new ColumnState(version, Utils.hash(colValue), isEchoPending);
    ColumnState currColState = this.rowState.getColumnState(stateID);

    if (isEchoPending != null) {
      isEchoPending.forEach((source, isPending) -> {
        // If there is a new echo pending, increment the counter.
        if (isPending) {
          context.counter("pending_column_echoes", CounterParam.of("source", source)).inc();
        }
      });
    }

    this.rowState.addColumnState(stateID, colState);
  }

  public String getState() { return this.rowState.toString(); }

  public void persistState() { context.mapPut(0, TABLE_STATE_KEY, Value.of(JsonStream.serialize(this.rowState))); }

  public boolean handleEcho(String tableName, String columnName, Map<String, Any> keysMap, String source,
                            String colVal) {
    String stateID = buildStateId(tableName, columnName, keysMap);
    ColumnState columnState = this.rowState.getColumnStates().get(stateID);
    if (columnState == null)
      return false;

    if (columnState.handleEchoForColumn(source, Utils.hash(colVal), context)) {
      // ECHO handled for column
      this.rowState.deleteColumnState(stateID);
      return true;
    }

    return false;
  }

  public String buildStateId(String tableName, String columnName, Map<String, Any> keysMap) {
    // Sort the keysMap by keys
    Map<String, Any> sortedMap = new TreeMap<>(keysMap);

    // Build the identifier string
    StringBuilder identifierBuilder = new StringBuilder();
    identifierBuilder.append(tableName).append("_").append(sortedMap.toString()).append("_").append(columnName);

    // Hash the identifier string
    return Utils.hash(identifierBuilder.toString());
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class RowState {
    private Map<String, ColumnState> columnStates = new HashMap<>();

    public void addColumnState(String column, ColumnState columnState) {
      if (columnStates.containsKey(column)) {
        // Add existing iEchoPending info to incoming column state.
        columnStates.get(column).isEchoPending.forEach((source, isEchoPending) -> {
          if (!columnState.isEchoPending.containsKey(source)) {
            columnState.isEchoPending.put(source, isEchoPending);
          }
        });
      }
      columnStates.put(column, columnState);
    }

    public ColumnState getColumnState(String column) { return columnStates.get(column); }

    public void deleteColumnState(String column) { columnStates.remove(column); }
  }

  @Data
  @NoArgsConstructor
  public static class ColumnState {
    private long version;
    private String changeId;
    private Map<String, Boolean> isEchoPending = new HashMap<>();

    public ColumnState(long version, String changeId, Map<String, Boolean> isEchoPending) {
      this.version = version;
      this.changeId = changeId;
      this.isEchoPending = isEchoPending == null ? new HashMap<>() : new HashMap<>(isEchoPending);
    }

    public boolean isEchoPending(String source) { return isEchoPending.getOrDefault(source, false); }

    boolean handleEchoForColumn(String source, String incomingChangeID, GrainContext context) {

      if (incomingChangeID.equals(changeId)) {
        isEchoPending.put(source, false);
        context.counter("received_column_echoes", CounterParam.of("source", source)).inc();
      } else {
        // new incoming changeId.
      }

      return !isEchoPending.values().contains(true);
    }
  }
}
