package org.samples.sql_to_sql;

public interface Constants {
  public final String GRAINITE_SOURCE_NAME = "grainite.source.name";
  public final String GRAINITE_DEST_NAME = "grainite.dest.name";
  public final String GRAINITE_ERROR_SINK_TOPIC = "grainite.err.topic";
  public final String ON_PREM_CONNECTION_URL = "onprem.sql.connection.string";
  public final String CLOUD_CONNECTION_URL = "cloud.sql.connection.string";
  public final String SOURCE_ONPREM_NAME = "onprem";
  public final String SOURCE_CLOUD_NAME = "cloud";
  public final String SQL_CONNECTION_USERNAME = "sql.connection.username";
  public final String SQL_CONNECTION_PASSWORD = "sql.connection.password";
}
