package org.samples.sql_to_sql;

import com.jsoniter.any.Any;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Utils {

  // get changes diff between before and after maps.
  public static Map<String, Any> getChangeDiff(Map<String, Any> after, Map<String, Any> before) {

    if (after == null) {
      return null;
    }

    if (before == null) {
      return new HashMap<>(after);
    }

    Map<String, Any> diffMap = new HashMap<>();
    for (Map.Entry<String, Any> entry : after.entrySet()) {
      String key = entry.getKey();
      Any afterValue = entry.getValue();
      Any beforeValue = before.get(key);

      if (!Objects.equals(afterValue, beforeValue)) {
        diffMap.put(key, afterValue);
      }
    }
    return diffMap;
  }

  public static String hash(String data) {
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      byte[] hashedBytes = md.digest(data.getBytes(StandardCharsets.UTF_8));
      return bytesToHex(hashedBytes);
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
      throw new IllegalStateException(e.getMessage());
    }
  }

  private static String bytesToHex(byte[] bytes) {
    StringBuilder result = new StringBuilder();
    for (byte b : bytes) {
      result.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
    }
    return result.toString();
  }

  public static int compareLSN(String lsn1, String lsn2) { return Long.compare(parseLSN(lsn1), parseLSN(lsn2)); }

  // lsn format {0000002c:00002d68:0003}
  private static long parseLSN(String lsn) {
    long value = 0;
    if (lsn == null || lsn.isEmpty())
      return value;

    for (String part : lsn.split(":")) {
      value = (value << 8) + Long.parseLong(part, 16);
    }
    return value;
  }

  public static void printLog(Logger logger, String message, Level logLevel, boolean print) {
    if (print) {
      logger.log(logLevel, message);
    }
  }
}
