package org.samples.sql_to_sql;

import com.github.javafaker.Business;
import com.github.javafaker.Faker;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.UUID;

public class Client {

  private static Random random = new Random();
  private static final String USERNAME = "SA";
  private static final String PASSWORD = "Password123";

  public static void main(String[] args) {

    String dbURL = args[0];
    int insertCount = Integer.parseInt(args[1]);

    // load JDBC driver class
    loadDriver();

    Connection connection = getDbConnection(dbURL, USERNAME, PASSWORD);

    try {

      connection.setAutoCommit(false);

      // create inserts
      while (insertCount-- > 0) {
        PreparedStatement statement = connection.prepareStatement(createInsert("cards"));
        statement.execute();
        connection.commit();
        System.out.println("Inserted new row into source.");
      }

      System.out.println("Inserts completed.");
    } catch (SQLException e) {
      try {
        System.out.println("Error encountered, rolling back changes...");
        connection.rollback();
      } catch (SQLException e1) {
        e1.printStackTrace();
        System.exit(1);
      }
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static Connection getDbConnection(String connectionURL, String username, String password) {
    Connection con = null;
    try {
      con = DriverManager.getConnection(connectionURL, username, password);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
    return con;
  }

  private static void loadDriver() {
    try {
      // load driver class
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static String createInsert(String tableName) {
    Business b = new Faker().business();

    String expiration = b.creditCardExpiry();
    expiration = expiration.substring(5, 7) + "/" + expiration.substring(2, 4);

    int randomInt = random.nextInt(99999999);

    return String.format("INSERT INTO cards (number, expiration, cvv, provider) VALUES (%s, '%s', '%s', '%s')",
                         randomInt, expiration, randomStr(3), b.creditCardType());
  }

  public static String randomStr(int length) {
    return UUID.randomUUID().toString().replaceAll("-", "").substring(0, length);
  }
}
