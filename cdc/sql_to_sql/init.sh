#!/bin/bash
echo "Pulling images"
docker-compose pull

echo "Launching docker containers."
docker-compose up -d

echo "Waiting for containers to start..."
while [[ "$(docker-compose ps --services | grep -E '^(sql_onprem|sql_cloud|grainite)$' | wc -l)" -ne 3 ]]; do
  sleep 1
done

echo "All containers are up. Initializing database."
sleep 8
./configure_db.sh
