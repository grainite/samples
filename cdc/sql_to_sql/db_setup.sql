USE Master
GO
drop database if exists testdb;
GO
CREATE DATABASE testdb;
GO
alter database testdb SET MULTI_USER WITH ROLLBACK IMMEDIATE;
GO
USE testdb;
GO
EXECUTE sys.sp_cdc_enable_db;
GO

USE testdb;
GO
/****** Create  Table [dbo].[cards] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cards]
(
    [number] [int] NOT NULL PRIMARY KEY,
    [expiration] [varchar](50) NULL,
    [cvv] [varchar](50) NULL,
    [provider] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Create  Table [dbo].[customers] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[customers]
(
    [Id] [int] NOT NULL,
    [Name] [varchar](50),
    [Age] [int] NOT NULL,
    PRIMARY KEY (Id,Age)
);
GO

EXECUTE sys.sp_cdc_enable_table @source_schema = N'dbo',
  @source_name = N'cards',
  @role_name = null,
  @capture_instance = N'cards';
GO

EXECUTE sys.sp_cdc_enable_table @source_schema = N'dbo',
  @source_name = N'customers',
  @role_name = null,
  @capture_instance = N'customers';
GO