## SQL Server to SQL Server two way sync

## Environment Setup (Prerequistes)

**NOTE**: Follow the [instructions in Grainite documentation] (https://docs.grainite.com/developers/getting-started/environment-setup) for detailed instructions to setup the environment.

## Install 'gx', Grainite Command Line Interface

1. Download the gx CLI installer script from here - https://gitlab.com/api/v4/projects/41132986/releases/permalink/latest/downloads/gx-bin/install_gx.sh
2. Run the installer script:
```bash
chmod +x install_gx.sh && sudo ./install_gx.sh
```
3. `gx` should have installed at `/usr/local/bin`. To confirm, run the following:
```
$> gx --version

2326.12

```

### Install Java

Java is needed to run 'mvn' to build the applications, as well as to run the application. 

### Install 'docker' and 'docker-compose'

Docker compose standalone is needed to start Grainite, as well as the two sql instances. Follow the instructions on [this page to install docker desktop or the docker engine](https://docs.docker.com/get-docker/).

## Start the Docker containers

Run the following command to start Grainite and the two sql instances (sql_onprem and sql_cloud).

```bash
$> ./init.sh

Launching docker containers.
Creating network "sql_to_sql_cdc_sql-network" with driver "bridge"
Creating grainite   ... done
Creating sql_onprem ... done
Creating sql_cloud  ... done
Waiting for containers to start...
All containers are up. Initializing database.
Setting onprem database
Changed database context to 'master'.
Changed database context to 'testdb'.
Changed database context to 'testdb'.
Update mask evaluation will be disabled in net_changes_function because the CLR configuration option is disabled.
Job 'cdc.testdb_capture' started successfully.
Job 'cdc.testdb_cleanup' started successfully.
Setting up cloud database
Changed database context to 'master'.
Changed database context to 'testdb'.
Changed database context to 'testdb'.
Update mask evaluation will be disabled in net_changes_function because the CLR configuration option is disabled.
Job 'cdc.testdb_capture' started successfully.
Job 'cdc.testdb_cleanup' started successfully.
```

## Edit the app.yaml file and build the application

1. Edit the app.yaml and configure each <sql_onprem_ip> and <sql_cloud_ip> field to reflect the correct IP address of the two Onprem and Cloud SQL server instances. You may use the IP Address of the virtual machine for running this sample.

2. Run the follwoing command to build the application
```bash
$> mvn clean package
```

3. Once the app is built, run the following to load the app into Grainite:
```bash
gx load -c app.yaml
```

## Start the Tasks

Run the following commands to start onprem_task and cloud_task:

```bash
$> gx task start onprem_task -c app.yaml
[WARNING] Certificates were not found under /home/<user>/samples/cer
ts. Running without TLS.
[SUCCESS] Server is ready
Running gx task start with options:
Task Name: onprem_task
      App: sql_to_sql_cdc

[SUCCESS] Task Started
```

```bash
$> gx task start cloud_task -c app.yaml
[WARNING] Certificates were not found under /home/<user>/samples/cer
ts. Running without TLS.
[SUCCESS] Server is ready
Running gx task start with options:
Task Name: cloud_task
      App: sql_to_sql_cdc

[SUCCESS] Task Started
```

To verify both are working, run the following command to see the application counters.

```bash
$> gx mon
                             default:sql_to_sql_cdc
                                  Message Flow
  SOURCE      TARGET    NUM MESSAGES      p50       p95      p99
task_table  task_table        8            -         -        -
                                 Action Status
            ACTION                TABLE     ACTION TYPE   COUNT
      cloud_task_doWork        task_table    task event    123
     cloud_task_startTask      task_table   sync invoke     1
 cloud_task_startTaskInstance  task_table  grain request    2
      onprem_task_doWork       task_table    task event    268
    onprem_task_startTask      task_table   sync invoke     1
onprem_task_startTaskInstance  task_table  grain request    2
                                  App Counters
       COUNTER         VALUE
  controller_created     4
task_execution_status    2
 task_instance_start     4

```

The `Message Flow` section in the `gx mon` output indicates how many events were appended to a topic, and tracks the flow of messages between topics and tables.<br>

The `Action Status` section indicates how many times an action was invoked.<br>

For example, `onprem_task_doWork` and `cloud_task_doWork` indicate that these actions were invoked 268 and 123 times, respectively.<br>
You will see the doWork action gets invoked frequently to poll for changes on the sql instances.
The startTask (used to start a task) and startTaskInstance (used to start a task instance) action counters indicate how many times those methods were called.<br>

Finally, we have the App Counters section which contains app specific counters. These counters can be included by users in any app by using the counters/gauges API.

This app's `controller_created` counter indicates how many times a controller was created to poll the sql instances.<br>
The `task_execution_status` gauge indicates the status of a task. Use `curl localhost:5064/export-dashboard | grep "task_execution_status"` to see the label for this gauge.<br>
The `task_instance_start` counter indicates the number of times a task instance was started.<br>


To exit out of `gx mon`, simply press  `ctrl + c`.


## Now you are ready to populate the SQL database with data and observe the CDC.

## Add entries to the cards table

Run the following script to generate 10 random inserts into cards table on a `sql_onprem` db instance.

```bash
./run.sh "jdbc:sqlserver://<sql_onprem_ip>:1433;databaseName=testdb;encrypt=true;trustServerCertificate=true" 10
format: ./run.sh <dbConnectionURL> <insert_count>
```

Confirm records were inserted into `sql_onprem` by running the following commands:
```bash
docker exec -it sql_onprem /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Password123
use testdb
GO
select * from cards;
GO
```
Run gx mon to see counter changes when the inserted rows are synced to `sql_cloud.cards`(target table)

```bash
$> gx mon

                                      default:sql_to_sql_cdc
                                           Message Flow
  SOURCE        TARGET     NUM MESSAGES    p50      p95     p99
task_table    cdc_topic          6          -        -       -
task_table    task_table         8          -        -       -
 cdc_topic  cdc_processor        6         4 ms    4 ms    4 ms
                                       Action Handler Status
  ACTION HANDLER        TABLE      NUM. ACTIONS       p50          p95         p99
SQLRecordProcessor  cdc_processor        6        922.494 ms   922.494 ms  922.494 ms
                                           Action Status
            ACTION                 TABLE       ACTION TYPE   COUNT
        processRecords         cdc_processor   topic event     6
      cloud_task_doWork          task_table     task event     56
     cloud_task_startTask        task_table    sync invoke     1
 cloud_task_startTaskInstance    task_table   grain request    2
      onprem_task_doWork         task_table     task event     61
    onprem_task_startTask        task_table    sync invoke     1
onprem_task_startTaskInstance    task_table   grain request    2
                                           App Counters
        COUNTER         VALUE
  controller_created      4
 pending_column_echoes    40
received_column_echoes    40
   received_records       20
     sent_records         20
 task_execution_status    2
  task_instance_start     4

```
We inserted 10 rows into sql_onprem.cards table and each row has 4 columns<br>
1. `received_records` indicates total number of records polled from source table, has value 20 (10 for source changes, 10 for echoes).<br>
2. `sent_records` indicates total number of records sent to grainite topic/table, has value 20 (10 for source changes, 10 for echoes).<br>
3. `pending_column_echoes` indicates the total no of echoes pending at each column level, has value 40 (10 rows * 4 columns).<br>
4. `received_column_echoes` indicates the total no of echoes received back, has value 40 (10 rows * 4 columns).


To verify that the records are synced to `sql_cloud` instance, run the following command.
```bash
docker exec -it sql_cloud /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P Password123
use testdb
GO
select * from cards;
GO
```
## Connecting to your own SQL Instances

To connect to your own sql instances, change the below properties in the `app.yaml`.
```yaml
...
tasks:
  - task_name: onprem_task
    config:
      ...
      connectionUrl: <connection string for sql_onprem>
      database.dbname: <sql_onprem_db_name>
      tables.property.name: collection.include.list
      collection.include.list: <sql tables to watch (comma separated)>
      ...
  - task_name: cloud_task
    config:
      ...
      connectionUrl: <connection string for sql_onprem>
      database.dbname: <sql_onprem_db_name>
      tables.property.name: collection.include.list
      collection.include.list: <sql tables to watch (comma separated)>
      ...
...
tables:
  - table_name: cdc_processor
    ...
    action_handlers:
      - name: SQLRecordProcessor
        ...
        config:
          onprem.sql.connection.string: <connection string for sql_onprem db>
          cloud.sql.connection.string: <connection string for sql_cloud db>
          sql.connection.username: <db_username>
          sql.connection.password: <db_password>
...
```
