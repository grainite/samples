#!/bin/bash

BASEDIR=$(cd "$(dirname "$0")"; pwd)

if [ "$#" -ne 5 ]; then
  echo "Usage: $0 <mongo connection string> <database name> <collection name> <num events to send> <target event rate>"
  echo "  eg: $0 mongodb://localhost:27017 test customers 100 20"
  exit 1
fi

java -cp ${BASEDIR}/target/mongocdc-jar-with-dependencies.jar org.samples.mongocdc.Client $@

