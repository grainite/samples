# Mongo 2 Way Sync

- [Mongo 2 Way Sync](#mongo-2-way-sync)
  - [Env Setup](#env-setup)
    - [Install `gx`](#install-gx)
    - [Install Java](#install-java)
    - [Install `docker` and `docker-compose`](#install-docker-and-docker-compose)
  - [Start the Docker containers](#start-the-docker-containers)
  - [Build and load the Application](#build-and-load-the-application)
  - [Start the Tasks](#start-the-tasks)
  - [Add entries to the Mongo instances](#add-entries-to-the-mongo-instances)
  - [Connecting to your own Mongo Instances](#connecting-to-your-own-mongo-instances)

## Env Setup

**NOTE**: Follow the [instructions in the docs](https://gitbook.grainite.com/developers/getting-started/environment-setup) for more detailed instructions to setup the environment.

### Install `gx`

1. If gx is not already installed, download the gx CLI installer script from here - https://gitlab.com/api/v4/projects/41132986/releases/permalink/latest/downloads/gx-bin/install_gx.sh
2. Run the installer script:
```bash
chmod +x install_gx.sh && sudo ./install_gx.sh
```
3. `gx` should have installed at `/usr/local/bin`. To confirm, run the following:
```
$> gx --version

2324
```

### Install Java 

Java is needed to run `mvnw` to build the app, as well as to run the app.

### Install `docker` and `docker-compose`

Docker compose is needed to start Grainite, as well as the two Mongo instances. Follow the instructions on [this page to install docker desktop or the docker engine](https://docs.docker.com/get-docker/).


## Start the Docker containers

Run the following command to start Grainite and the two Mongo instances. Note: Some users may already have a grainite container running which will cause an error when docker tries to create that same container. If so, the error message may be ignored.
```bash
$> docker-compose pull && docker-compose up -d
Pulling mongo_one         ... done
Pulling mongo_one_init_rs ... done
Pulling mongo_two         ... done
Pulling mongo_two_init_rs ... done
Pulling grainite          ... done
Creating network "mongo_cdc_mongo-network" with driver "bridge"
Creating mongo_one ... done
Creating grainite       ... done
Creating mongo_two      ... done
Creating mongo_one_init ... done
Creating mongo_two_init ... done
```

## Build and load the Application

1. Run the following command to build the app:
```bash
./mvnw clean package
```
2. Once the app is built, run the following to load the app into Grainite:
```bash
gx load -c app.yaml
```

## Start the Tasks

Run the following commands to have the tasks poll both the Mongo instances for changes:
```bash
$> gx task start mongo_cdc_one -c app.yaml

Running gx task start with options:
      App: mongo_cdc
Task Name: mongo_cdc_one

[SUCCESS] Task Started
```
```bash
$> gx task start mongo_cdc_two -c app.yaml

Running gx task start with options:
      App: mongo_cdc
Task Name: mongo_cdc_two

[SUCCESS] Task Started
```

To confirm that both the tasks are working, run the following to see counters:
```
$> gx mon

                             default:mongo_cdc
                               Message Flow
  SOURCE      TARGET    NUM MESSAGES    p50      p95     p99
task_table  task_table        4          -        -       -
                               Action Status
             ACTION                 TABLE     ACTION TYPE   COUNT
      mongo_cdc_one_doWork       task_table    task event     39
    mongo_cdc_one_startTask      task_table   sync invoke     1
mongo_cdc_one_startTaskInstance  task_table  grain request    1
      mongo_cdc_two_doWork       task_table    task event     28
    mongo_cdc_two_startTask      task_table   sync invoke     1
mongo_cdc_two_startTaskInstance  task_table  grain request    1
                               App Counters
         COUNTER          VALUE
gxcdc_controller_created    2
 gxtask_execution_status    2
  gxtask_instance_start     2
```

You will see the `doWork` action gets invoked frequently to poll for changes.

To exit out of `gx mon`, simply press  `ctrl + c`.

## Add entries to the Mongo instances

Run the following command to add some dummy data to the databases.

1. Run the following to add some dummy data to `mongo_one`'s `test` db and `customers` collection:
```bash
./run.sh mongodb://localhost:27017 test customers 10 10
```
2. Confirm records were inserted into `mongo_one` by running the following command to dump the collection:
```bash
docker exec -it mongo_one mongosh --eval "db.getSiblingDB('test').getCollection('customers').find()"
```
3. Additionally, confirm records were synced into `mongo_two` by running the following command to dump the collection:
```bash
docker exec -it mongo_two mongosh --eval "db.getSiblingDB('test').getCollection('customers').find()"
```
4. Run the following to add some dummy data to `mongo_two`'s `test` db and `customers` collection:
```bash
./run.sh mongodb://localhost:27018 test customers 10 10
```
5. Confirm records were inserted into `mongo_two` by running the following command to dump the collection:
```bash
docker exec -it mongo_two mongosh --eval "db.getSiblingDB('test').getCollection('customers').find()"
```
6. Additionally, confirm records were synced into `mongo_one` by running the following command to dump the collection:
```bash
docker exec -it mongo_one mongosh --eval "db.getSiblingDB('test').getCollection('customers').find()"
```

**Note**: To get the number of documents in a collection, you can run the following:
```bash
docker exec -it mongo_one mongosh --eval "db.getSiblingDB('test').getCollection('customers').countDocuments()"
```
## Connecting to your own Mongo Instances

To connect to your own Mongo instances, change the connection strings in the `app.yaml` to point to your mongo instances, and the database and collection names.
```yaml
...
tasks:
  - task_name: mongo_cdc_one
    config:
      ...
      mongodb.connection.string: <connection string for first mongo instance replica set>
      mongodb.name: <mongo db name>
      mongodb.collection: <mongo collections (comma separated)>
      collection.include.list: <collections to watch>
      ...
  - task_name: mongo_cdc_two
    config:
      ...
      mongodb.connection.string: <connection string for second mongo instance replica set>
      mongodb.name: <mongo db name>
      mongodb.collection: <mongo collections (comma separated)>
      collection.include.list: <collections to watch>
      ...
...
tables:
  - table_name: cdc_processor
    ...
    action_handlers:
      - name: MongoRecordProcessor
        ...
        config:
          one.mongodb.connection.string: <connection string for first mongo instance>
          two.mongodb.connection.string: <connection string for second mongo instance>
...
```
