package org.samples.mongocdc;

import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.jsoniter.JsonIterator;
import com.jsoniter.output.JsonStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

public class StateManager {

  private static final Key DOCUMENT_STATE_KEY = Key.of("DOCUMENT_STATE");

  private GrainContext context;
  private DocumentState documentState;

  public StateManager(GrainContext context) {
    this.context = context;
    if (context.mapKeyExists(0, DOCUMENT_STATE_KEY)) {
      this.documentState =
          JsonIterator.deserialize(context.mapGet(0, DOCUMENT_STATE_KEY).asString()).as(DocumentState.class);
    } else {
      this.documentState = new DocumentState();
    }
  }

  public void insertKey(String key, long version, boolean isDelete, Map<String, Boolean> isEchoPending) {

    // Avoid storing GR_VERSION_KEY in state.
    if (key.startsWith(TransactionBuilder.GR_VERSION_KEY))
      return;
    KeyState keyState = new KeyState(version, isDelete, isEchoPending);

    KeyState currKeyState = this.documentState.getKeyState(key);

    // Set counters for echoes.
    if (isEchoPending != null) {
      isEchoPending.forEach((source, isPending) -> {
        // If there is a new echo pending, increment the counter.
        if (isPending && currKeyState == null) {
          context.counter("pending_echoes", CounterParam.of("source", source)).inc();
        }
      });
    }

    this.documentState.addKeyState(key, keyState);
  }

  public @Nullable KeyState getKey(String key) { return this.documentState.getKeyState(key); }

  // Save the changes to grain map.
  public void persistState() {
    context.mapPut(0, DOCUMENT_STATE_KEY, Value.of(JsonStream.serialize(this.documentState)));
  }

  public void handleEcho(String source, long grVersion) {

    // Scan through all the keys for this document and check if echo is pending for the key from the source.
    Set<String> keysToDelete = new HashSet<>();

    this.documentState.getKeyStates().forEach((k, v) -> {
      if (v.handleEchoForKey(source, grVersion, k, context)) {
        keysToDelete.add(k);
      }
    });

    keysToDelete.forEach(this.documentState::deleteKeyState);
  }

  /**
   * Returns a ScanResult object with the following information:
   * 1. maxVersion: the max version of the key in the state.
   * 2. isEchoPending: a map of source to isEchoPending for the key.
   *
   * If no key part is present in the state, then null is returned.
   *
   * If `throwErrorIfDeleteKeyIsPresent` is true, then a DeleteKeyPartFoundException is thrown if a key part with delete
   * is present.
   *
   * @param key
   * @param throwErrorIfDeleteKeyIsPresent if a key part with delete is present, throw a DeleteKeyPartFoundException.
   * @return
   * @throws DeleteKeyPartFoundException
   */
  public @Nullable MaxKeyPart getMaxKeyPart(String key, boolean throwErrorIfDeleteKeyIsPresent)
      throws DeleteKeyPartFoundException {
    List<String> keyParts = getKeyParts(key);
    MaxKeyPart scanResult = new MaxKeyPart();
    for (String keyPart : keyParts) {
      KeyState keyState = this.documentState.getKeyState(keyPart);

      if (keyState != null && throwErrorIfDeleteKeyIsPresent && keyState.isDelete())
        throw new DeleteKeyPartFoundException("Delete key part found in state for key part: " + keyPart);

      scanResult.considerKey(this.documentState.getKeyState(keyPart));
    }

    return scanResult.getMaxVersion() == null ? null : scanResult;
  }

  static List<String> getKeyParts(String key) {
    String [] parts = key.split("\\.");
    List<String> result = new ArrayList<>(parts.length);
    StringBuilder currentKey = new StringBuilder(parts[0]);
    result.add(currentKey.toString());
    
    for (int i = 1; i < parts.length; i++) {
      String part = parts[i];
      currentKey.append(".").append(part);
      result.add(currentKey.toString());
    }
    return result;
  }

  @Data
  @AllArgsConstructor
  @NoArgsConstructor
  public static class DocumentState {
    private Map<String, KeyState> keyStates = new HashMap<>();

    public void addKeyState(String key, KeyState keyState) {
      if (keyStates.containsKey(key)) {
        // Add missing isEchoPending states to keyState.
        keyStates.get(key).isEchoPending.forEach((source, isEchoPending) -> {
          if (!keyState.isEchoPending.containsKey(source)) {
            keyState.isEchoPending.put(source, isEchoPending);
          }
        });
      }
      keyStates.put(key, keyState);
    }

    public KeyState getKeyState(String key) { return keyStates.get(key); }

    public void deleteKeyState(String key) { keyStates.remove(key); }
  }

  @Data
  @NoArgsConstructor
  public static class KeyState {
    private long version;
    private boolean isDelete;
    // set to true against the destination when issuing writes to mongo writer.
    private Map<String, Boolean> isEchoPending = new HashMap<>();

    public KeyState(long version, boolean isDelete, Map<String, Boolean> isEchoPending) {
      this.version = version;
      this.isDelete = isDelete;
      this.isEchoPending = isEchoPending == null ? new HashMap<>() : new HashMap<>(isEchoPending);
    }

    public boolean isEchoPending(String source) { return isEchoPending.getOrDefault(source, false); }

    /**
     * Return true if there are no pending echo requests for this key. This means that they key can be removed from the
     * document state.
     * @param source
     * @param grVersion
     * @param key
     * @return
     */
    boolean handleEchoForKey(String source, long grVersion, String key, GrainContext context) {
      if (grVersion == version) {
        isEchoPending.put(source, false);
        context.counter("received_echoes", CounterParam.of("source", source)).inc();
      } else {
        // grVersion is stale, nothing to do.
      }

      return !isEchoPending.values().contains(true);
    }
  }

  @Data
  public static class MaxKeyPart {
    private Long maxVersion = null;
    Map<String, Boolean> isEchoPending = new HashMap<>();

    public Boolean getIsEchoPendingForSource(String source) { return isEchoPending.getOrDefault(source, false); }

    void considerKey(KeyState keyState) {
      if (keyState == null)
        return;

      if (maxVersion == null || keyState.getVersion() > maxVersion)
        maxVersion = keyState.getVersion();
      keyState.getIsEchoPending().forEach((source, isEchoPending) -> {
        if (isEchoPending)
          this.isEchoPending.put(source, true);
      });
    }
  }

  public class DeleteKeyPartFoundException extends Exception {
    public DeleteKeyPartFoundException(String message) { super(message); }
  }
}
