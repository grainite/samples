package org.samples.mongocdc;

import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.grainite.api.context.PoolAllocator;
import com.grainite.gimpl.common.Pair;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import org.bson.Document;
import org.bson.conversions.Bson;

public class MongoWriter {

  // 20 * 5 = 100 connections per source.
  private static final int SOURCE_POOL_SIZE = 5;

  GrainContext context;
  StateManager stateManager;
  String databaseName;
  String collectionName;
  String key;

  private final static UpdateOptions updateOptions = new UpdateOptions().upsert(true);

  public MongoWriter(GrainContext context, StateManager stateManager) {
    Object[] keyParts = context.getKey().asKeyParts();
    if (keyParts.length < 2)
      throw new IllegalStateException("Invalid key passed to MongoWriter: " + context.getKey());

    this.context = context;
    this.stateManager = stateManager;
    this.databaseName = keyParts[0].toString();
    this.collectionName = keyParts[2].toString();
    this.key = keyParts[3].toString();
  }

  public void sendTransactions(TransactionBuilder tb) {
    for (Entry<String, List<Pair<Bson, Bson>>> entry : tb.buildTransactionsForAllSources().entrySet()) {
      String destination = entry.getKey();
      List<Pair<Bson, Bson>> updates = entry.getValue();

      final String sourceConnectionString = context.getConfig().get(destination + ".mongodb.connection.string");
      if (sourceConnectionString == null) {
        throw new IllegalStateException("Missing MongoDB connection string for source: " + destination);
      }

      int sourcePoolNum = Math.abs(key.hashCode() % SOURCE_POOL_SIZE);

      MongoClient client = context.getPooledObject(sourceConnectionString + sourcePoolNum, new PoolAllocator() {
        @Override
        public Object allocate() {
          return MongoClients.create(sourceConnectionString);
        }

        @Override
        public void deallocate(Object object) {
          ((MongoClient)object).close();
        }
      }, MongoClient.class);

      // context.getLogger().info("Starting transaction for " + collectionName + " in " + databaseName + " from " +
      //                          source + " @ " + sourceConnectionString);

      MongoCollection<Document> collection = client.getDatabase(databaseName).getCollection(collectionName);
      context.getLogger().info("Writing updates for " + collectionName + " in " + databaseName + " to " + destination);
      List<WriteModel<Document>> writeModels = new ArrayList<>();
      for (Pair<Bson, Bson> updatePair : updates) {
        // a -> filter, b -> updates
        writeModels.add(new UpdateOneModel<>(updatePair.a, updatePair.b, updateOptions));
        context.counter("doc_updates_issued", CounterParam.of("destination", destination)).inc();
      }

      context.counter("writes_issued", CounterParam.of("destination", destination)).inc();

      context.getLogger().info(collection.bulkWrite(writeModels).toString());
      context.getLogger().info("Sent writes for " + collectionName + " in " + databaseName + " to " + destination);
    }

    this.stateManager.persistState();
  }
}
