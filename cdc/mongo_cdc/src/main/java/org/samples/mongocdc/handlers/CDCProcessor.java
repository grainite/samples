package org.samples.mongocdc.handlers;

import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.Action;
import com.grainite.api.context.Action.TopicEvent;
import com.grainite.api.context.ActionResult;
import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.jsoniter.JsonIterator;
import com.jsoniter.ValueType;
import com.jsoniter.any.Any;
import com.jsoniter.output.JsonStream;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.samples.mongocdc.Constants;
import org.samples.mongocdc.ErrorSinkHandler;
import org.samples.mongocdc.ErrorSinkHandler.ErrorSink;
import org.samples.mongocdc.ErrorSinkHandler.SinkAction;
import org.samples.mongocdc.ErrorSinkHandler.SinkEventType;
import org.samples.mongocdc.MongoWriter;
import org.samples.mongocdc.StateManager;
import org.samples.mongocdc.StateManager.DeleteKeyPartFoundException;
import org.samples.mongocdc.StateManager.MaxKeyPart;
import org.samples.mongocdc.TransactionBuilder;
import org.samples.mongocdc.TransactionBuilder.OP;
import org.samples.mongocdc.Utils;

public class CDCProcessor {
  private GrainContext context = null;
  private ErrorSinkHandler errorSinkHandler = null;
  public List<ActionResult> processRecords(List<Action> actions, GrainContext context) throws Exception {
    List<ActionResult> results = new ArrayList<>();
    this.context = context;
    errorSinkHandler = new ErrorSinkHandler(context);
    TransactionBuilder txnBuilder = new TransactionBuilder();
    StateManager stateManager = new StateManager(context);
    MongoWriter writer = new MongoWriter(context, stateManager);

    Object[] keyParts = context.getKey().asKeyParts();
    String objectID = keyParts[keyParts.length - 1].toString();

    for (Action action : actions) {

      TopicEvent te = (TopicEvent)action;
      List<Any> recordList = getRecordsList(context, te.getPayload().asString());

      recordList.forEach(r -> {
        // extract payload info

        Map<String, Any> valuePayloadMap = r.get("value", "payload").asMap();
        Map<String, Any> incomingSourceInfo = valuePayloadMap.get("source").asMap();
        long dbOpTime = incomingSourceInfo.get("ts_ms").toLong();
        long debeziumTime = valuePayloadMap.get("ts_ms").toLong();
        long wallTime = incomingSourceInfo.get("wallTime").toLong();
        String source = r.get("grainite", Constants.GRAINITE_SOURCE_NAME).toString();
        String destination = r.get("grainite", Constants.GRAINITE_DEST_NAME).toString();
        String errTopic = r.get("grainite", Constants.GRAINITE_ERROR_SINK_TOPIC).toString();

        // set error topicName for errSinkHandler
        errorSinkHandler.setErrTopicName(errTopic);

        recordDebeziumLatency(dbOpTime, debeziumTime, source);

        context.counter("records", CounterParam.of("source", source)).inc();

        if (source.trim().length() == 0 || destination.trim().length() == 0) {
          throw new IllegalArgumentException(
              String.format("Configuration properties %s and %s must be configured in the source task",
                            Constants.GRAINITE_SOURCE_NAME, Constants.GRAINITE_DEST_NAME));
        }

        // get incoming op-code
        String opCode = valuePayloadMap.get("op").toString();
        String after =
            valuePayloadMap.get("after") == null || valuePayloadMap.get("after").valueType().equals(ValueType.NULL)
                ? null
                : valuePayloadMap.get("after").toString();

        context.counter("ops", CounterParam.of("op_type", opCode)).inc();

        switch (opCode) {
        case "c":
          // Document CREATE case
          // forward incoming event to destination and modify isEchoPending state
          // save modified object state
          // Extract incoming fields from 'after'

          try {
            processCDCData(objectID, after, after, wallTime, stateManager, txnBuilder, source, destination);
          } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage(), e);
          }

          break;
        case "u":
          // Document UPDATE case
          // The incoming events changed fields is present in
          // 1. removedFields (set only when a property is removed)
          // 2. updatedFields (has all the modified property info)
          Map<String, Any> updateDesc = valuePayloadMap.get("updateDescription").asMap();
          String updatedFields = updateDesc.get("updatedFields").toString();

          List<Any> removedFieldList = updateDesc.containsKey("removedFields")
                                           ? updateDesc.get("removedFields").asList()
                                           : Collections.emptyList();

          if (removedFieldList != null && removedFieldList.size() > 0) {
            // deleted fields processing
            // check if these fields are present in the state
            // if not present
            // if present
            // and is deleted, not deleted
            removedFieldList.forEach(fieldName -> {
              processDeletes(objectID, updatedFields, fieldName.toString(), wallTime, stateManager, txnBuilder, source,
                             destination);
            });
          }

          // un-deleted fields processing
          try {
            processCDCData(objectID, updatedFields, after, wallTime, stateManager, txnBuilder, source, destination);
          } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage(), e);
          }

          break;
        case "d":
          context.getLogger().info("Document delete events not implemented yet.");
          return;
        case "r":
          break;
        default:
          throw new IllegalArgumentException("Invalid opCode : " + opCode);
        }
      });

      results.add(ActionResult.success(action));
    }

    // Issue all mongo writes at the end of the batch.
    writer.sendTransactions(txnBuilder);

    return results;
  }

  private void processCDCData(String objectID, String changedData, String after, long incomingVersion,
                              StateManager stateManager, TransactionBuilder txnBuilder, String source,
                              String destination) throws IOException {

    Map<String, Object> dataMap = Utils.buildFlatKeys(after);
    Map<String, Object> changedKeysMap = Utils.buildFlatKeys(changedData);
    Long grVersion = (Long)changedKeysMap.get(TransactionBuilder.GR_VERSION_KEY);

    if (grVersion != null) {
      // Event is an echo event; process it and continue to next field.
      stateManager.handleEcho(source, grVersion);
      return;
    }

    changedKeysMap.entrySet().stream().forEach(field -> {
      String key = field.getKey();
      Object value = dataMap != null ? dataMap.get(key) : null;
      if (value == null)
        value = field.getValue();

      try {

        MaxKeyPart maxKeyPart = stateManager.getMaxKeyPart(key, false);
        if (Objects.isNull(maxKeyPart)) {
          // keypart NOT_FOUND; create case
          // insert key into SM and set destination isEchoPending:true
          context.getLogger().info("Max key part not found for key : " + key);
          stateManager.insertKey(key, incomingVersion, false, Collections.singletonMap(destination, true));

          // add write to txn builder for sending to destination.
          txnBuilder.addWrites(destination, objectID, key, value, OP.SET, incomingVersion);
          return;
        } else {

          // keypart found
          long storedVersion = maxKeyPart.getMaxVersion();

          if (incomingVersion == storedVersion) {
            // Only possible if we have already considered a parent within this event.
            // In that case, skip.
            return;
          }

          if (incomingVersion < storedVersion) {
            context.getLogger().info(String.format("Incoming version %d is not latest for %s in object %s, ignoring.",
                                                   incomingVersion, key, objectID));
            context.counter("conflict", CounterParam.of("conflict_type", "stale")).inc();
            // Error sink stale event and ignore the field
            ErrorSink errSink = ErrorSink.builder()
                                    .topicKey(objectID)
                                    .eventType(SinkEventType.STALE_EVENT)
                                    .actionTaken(SinkAction.SKIP_EVENT)
                                    .version(incomingVersion)
                                    .build();
            errorSinkHandler.sink(errSink, source);
            return;
          }

          context.getLogger().info(
              String.format("Incoming version %d is latest for %s in object %s", incomingVersion, key, objectID));
          if (incomingVersion > storedVersion) {
            context.counter("conflict", CounterParam.of("conflict_type", "resolved")).inc();
          }

          // incoming event is latest, forward it to other source
          stateManager.insertKey(key, incomingVersion, false, Collections.singletonMap(destination, true));

          // add write to txn builder for sending to destination.
          txnBuilder.addWrites(destination, objectID, key, value, OP.SET, incomingVersion);

          // send back to source check?
          Boolean isEchoPendingOnSource = maxKeyPart.getIsEchoPendingForSource(source);

          if (isEchoPendingOnSource) {
            // We have already issued a stale write to the source, therefore we need to update it
            // with latest. Add write to txn builder for sending back to source.
            stateManager.insertKey(key, incomingVersion, false, Collections.singletonMap(source, true));

            txnBuilder.addWrites(source, objectID, key, value, OP.SET, incomingVersion);
          }
        }
      } catch (DeleteKeyPartFoundException e) {
        ErrorSink errSink = ErrorSink.builder()
                                .topicKey(objectID)
                                .eventType(SinkEventType.EVENT_ON_DELETED_KEY)
                                .actionTaken(SinkAction.SKIP_EVENT)
                                .version(incomingVersion)
                                .build();
        errorSinkHandler.sink(errSink, source);
        e.printStackTrace();
      }
    });
  }

  private void processDeletes(String objectID, String updatedField, String key, long incomingVersion,
                              StateManager stateManager, TransactionBuilder txnBuilder, String source,
                              String destination) {

    Map<String, Object> dataMap = Utils.buildFlatKeys(updatedField);
    Long grVersion = (Long)dataMap.get(TransactionBuilder.GR_VERSION_KEY);
    context.getLogger().info("DELETE_PROCESSING for KEY: " + key + "dataMap: " + JsonStream.serialize(dataMap));
    if (grVersion != null) {
      // Event is an echo event, process it and continue to next field.
      stateManager.handleEcho(source, grVersion);
      return;
    }

    try {
      MaxKeyPart maxKeyPart = stateManager.getMaxKeyPart(key, true);

      if (Objects.isNull(maxKeyPart)) {
        // key not found in state add key to TxnBuilder
        stateManager.insertKey(key, incomingVersion, true, Collections.singletonMap(destination, true));
        txnBuilder.addWrites(destination, objectID, key, null, OP.UNSET, incomingVersion);
        return;
      }

      // key is found and not in deleted state
      // compare time stamps
      long storedVersion = maxKeyPart.getMaxVersion();
      if (incomingVersion == storedVersion) {
        // Only possible if we have already considered a parent within this event.
        // In that case, skip.
        return;
      }

      if (incomingVersion > storedVersion) {
        context.getLogger().info(String.format("Incoming version %d is latest for %s in object %s, ignoring.",
                                               incomingVersion, key, objectID));
        context.counter("conflict", CounterParam.of("conflict_type", "resolved")).inc();

        // forward it to destination and update echo flags
        stateManager.insertKey(key, incomingVersion, true, Collections.singletonMap(destination, true));
        txnBuilder.addWrites(destination, objectID, key, null, OP.UNSET, incomingVersion);

        // if echo pending is true then send it back to the source
        Boolean isEchoPendingOnSource = maxKeyPart.getIsEchoPendingForSource(source);
        if (isEchoPendingOnSource) {
          // adding to keyToSource map to send message to source
          stateManager.insertKey(key, incomingVersion, true, Collections.singletonMap(source, true));
          txnBuilder.addWrites(source, objectID, key, null, OP.UNSET, incomingVersion);
        }
        return;
      }

      context.getLogger().info(String.format("Incoming version %d is not latest for %s in object %s, ignoring.",
                                             incomingVersion, key, objectID));
      context.counter("conflict", CounterParam.of("conflict_type", "stale")).inc();

      // incoming version is older
      // ignore the incoming version and Error sink
      ErrorSink errSink = ErrorSink.builder()
                              .topicKey(objectID)
                              .eventType(SinkEventType.STALE_DELETE_EVENT)
                              .actionTaken(SinkAction.SKIP_EVENT)
                              .version(incomingVersion)
                              .build();
      errorSinkHandler.sink(errSink, source);

    } catch (DeleteKeyPartFoundException e) {
      // key is marked deleted in stored object state
      // ignore the key and Error sink it
      ErrorSink errSink = ErrorSink.builder()
                              .topicKey(objectID)
                              .eventType(SinkEventType.EVENT_ON_DELETED_KEY)
                              .actionTaken(SinkAction.SKIP_EVENT)
                              .version(incomingVersion)
                              .build();
      errorSinkHandler.sink(errSink, source);
      e.printStackTrace();
    }
  }

  private void recordDebeziumLatency(long dbOpTime, long debeziumTime, String source) {
    Map<Object, Object> currentDebeziumAvgs;
    if (context.getValue().isNull()) {
      currentDebeziumAvgs = new HashMap<>();
    } else {
      currentDebeziumAvgs = context.getValue().asMap();
    }

    Duration duration = Duration.between(Instant.ofEpochMilli(dbOpTime), Instant.ofEpochMilli(debeziumTime));
    long currentTime = duration.toNanos();
    Long numInvocations = (Long)currentDebeziumAvgs.getOrDefault(source + ".num_invocations", 1L);
    Long avgTime = (Long)currentDebeziumAvgs.getOrDefault(source + ".average_time", 0L);
    final double smoothingFactor = 0.5;

    /**
     * Exponential Moving Average
     * EMAt = α * Vt + (1 - α) * EMAt-1
      where:
        •	EMAt is the EMA at time t
        •	Vt is the value at time t
        •	α is the smoothing factor, which determines the weight given to the most recent value (typically between
0 and 1)
        •	EMAt-1 is the EMA at the previous time step (t-1)
     */

    avgTime = (long)(smoothingFactor * currentTime + (1.0 - smoothingFactor) * avgTime);

    context.gauge("ema_debezium_latency_ns", CounterParam.of("source", source)).set(avgTime);
    context.gauge("debezium_latency_ns", CounterParam.of("source", source)).set(currentTime);

    currentDebeziumAvgs.put(source + ".num_invocations", numInvocations);
    currentDebeziumAvgs.put(source + ".average_time", avgTime);

    context.setValue(Value.of(currentDebeziumAvgs));
  }

  /**
   * Handles Debezium events and returns a list of records.
   * Also updates the grain state for storing partial events.
   *
   * @param context
   * @param incomingEvent
   * @return
   * @throws IOException
   */
  private List<Any> getRecordsList(GrainContext context, String incomingEvent) throws IOException {
    List<Any> finalRecordsList = new ArrayList<>();
    List<Any> recordList = JsonIterator.parse(incomingEvent).readAny().asList();
    for (Any record : recordList) {
      if (record.get("totalChunks").valueType() != ValueType.INVALID) {
        // Is a chunked record.
        String eventId = record.get("id").toString();
        int totalChunks = record.get("totalChunks").toInt();
        int chunkNum = record.get("chunkNum").toInt();
        String payload = record.get("payload").toString();
        if (chunkNum != totalChunks) {
          // More chunks are pending.
          context.mapPut(1, Key.of(eventId, chunkNum), Value.of(payload));
          context.getLogger().info(String.format("Received chunk %d of %d for %s.", chunkNum, totalChunks, eventId));
          // All chunks aren't available yet, don't add to record list.
        } else {
          // All chunks have been received, assemble the entire event.
          context.getLogger().info(String.format("=== All chunks received for %s ===", eventId));
          StringBuilder sb = new StringBuilder();
          for (int i = 0; i < totalChunks - 1; i++) {
            Key chunkKey = Key.of(eventId, i + 1);
            if (!context.mapKeyExists(1, chunkKey)) {
              throw new RuntimeException(String.format("Missing chunk %d for event %s.", i + 1, eventId));
            }
            sb.append(context.mapGet(1, chunkKey).asString());
            context.mapDelete(1, chunkKey);
          }
          // Add final chunk.
          sb.append(payload);
          // Decode base64 encoded string.
          String payloadStr = new String(Base64.getDecoder().decode(sb.toString()));
          finalRecordsList.add(JsonIterator.deserialize(payloadStr));
          context.counter("num_chunked_records_built").inc();
        }
      } else {
        // Is a complete record.
        finalRecordsList.add(record);
      }
    }

    return finalRecordsList;
  }
}
