package org.samples.mongocdc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.bson.Document;

public final class Utils {

  public static Map<String, Object> buildFlatKeys(String json) {
    if (json == null || json.isEmpty())
      return Collections.emptyMap();
    Document document = Document.parse(json);
    Map<String, Object> output = new LinkedHashMap<>();
    traverseDoc(document, output, null);
    return output;
  }

  private static void traverseDoc(Document doc, Map<String, Object> resultMap, String currentKey) {
    for (Entry<String, Object> entry : doc.entrySet()) {
      String key = currentKey == null ? entry.getKey() : currentKey + "." + entry.getKey();
      Object value = entry.getValue();

      if (key.equals("_id")) {
        continue;
      }

      resultMap.put(key, value);

      if (value instanceof Document) {
        traverseDoc((Document)value, resultMap, key);
      } else if (value instanceof ArrayList) {
        int i = 0;
        for (Object o : (ArrayList)value) {
          if (o instanceof Document) {
            traverseDoc((Document)o, resultMap, key + "." + i);
          }
          i++;
        }
      }
    }
  }
}
