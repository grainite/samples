package org.samples.mongocdc;

import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.CounterParam;
import com.grainite.api.context.GrainContext;
import com.jsoniter.output.JsonStream;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorSinkHandler {
  private GrainContext context;
  private String errTopicName;

  public ErrorSinkHandler(GrainContext context) { this.context = context; }

  public void sink(ErrorSink errorSink, String source) {
    String payload = JsonStream.serialize(errorSink);
    context.counter("err_sink", CounterParam.of("source_total_count", source)).inc();
    pushToErrorSink(context, Key.of(errorSink.getTopicKey()), Value.of(payload));
  }

  private void pushToErrorSink(GrainContext context, Key topicKey, Value payload) {
    this.context.sendToTopic(this.errTopicName, topicKey, payload, null);
  }

  @Data
  @Builder
  public static class ErrorSink {
    private String topicKey;
    private SinkEventType eventType;
    private SinkAction actionTaken;
    private Long version;
  }

  public static enum SinkEventType { STALE_EVENT, STALE_DELETE_EVENT, EVENT_ON_DELETED_KEY }

  public static enum SinkAction { SKIP_EVENT }
}
