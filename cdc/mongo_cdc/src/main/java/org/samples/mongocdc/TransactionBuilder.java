package org.samples.mongocdc;

import com.grainite.gimpl.common.Pair;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import com.mongodb.client.model.Updates;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

public class TransactionBuilder {

  public final static String GR_VERSION_KEY = "_grVersion";

  // Source - (documentId, updates)
  private Map<String, Map<String, DocumentUpdates>> sourceToDocUpdates = new HashMap<>();

  public void addWrites(String source, String objectId, String field, Object value, OP op, long grVersion) {
    Map<String, DocumentUpdates> docUpdatesMap = sourceToDocUpdates.get(source);
    if (docUpdatesMap == null) {
      docUpdatesMap = new HashMap<>();
      sourceToDocUpdates.put(source, docUpdatesMap);
    }

    DocumentUpdates docUpdates = docUpdatesMap.get(objectId);
    if (docUpdates == null) {
      docUpdates = new DocumentUpdates();
      docUpdatesMap.put(objectId, docUpdates);
    }

    docUpdates.addUpdate(grVersion, field, new FieldUpdate(value, grVersion, op));
  }

  // Source -> [Filter -> BsonUpdates]
  public Map<String, List<Pair<Bson, Bson>>> buildTransactionsForAllSources() {
    Map<String, List<Pair<Bson, Bson>>> sourceToUpdates = new HashMap<>();

    this.sourceToDocUpdates.forEach((source, docUpdatesMap) -> {
      List<Pair<Bson, Bson>> sourceUpdates = new ArrayList<>();
      sourceToUpdates.put(source, sourceUpdates);

      docUpdatesMap.forEach((objectId, documentUpdates) -> {
        List<Bson> updates = new ArrayList<>();
        updates.add(Updates.set(GR_VERSION_KEY, documentUpdates.grVersion));
        documentUpdates.fieldUpdatesMap.forEach((field, fieldUpdate) -> {
          updates.add(fieldUpdate.op == OP.SET ? Updates.set(field, fieldUpdate.value) : Updates.unset(field));
        });
        sourceUpdates.add(pairOf(new Document("_id", getObjectId(objectId)), Updates.combine(updates)));
      });
    });

    this.sourceToDocUpdates.clear();
    return sourceToUpdates;
  }

  // Reference - https://debezium.io/documentation/reference/stable/connectors/mongodb.html
  protected static Object getObjectId(String rawObjectId) {
    Any serialzedJson = JsonIterator.deserialize(rawObjectId);
    String normalizedObjectId = serialzedJson.toString();
    if (rawObjectId.startsWith("{")) {
      Set<String> keySet = serialzedJson.keys();
      // ObjectId
      if (keySet.contains("$oid")) {
        return new ObjectId(serialzedJson.get("$oid").toString());
      }

      // Binary
      if (keySet.contains("$binary") && keySet.contains("$type")) {
        throw new IllegalStateException("Binary ObjectId not supported, yet.");
      }

      return Document.parse(normalizedObjectId);
    } else {
      // Primitive objectId, check if String, Integer or Double
      if (rawObjectId.startsWith("\"")) {
        return normalizedObjectId;
      } else if (rawObjectId.contains(".")) {
        return Double.parseDouble(normalizedObjectId);
      } else {
        return Integer.parseInt(normalizedObjectId);
      }
    }
  }

  private static Pair<Bson, Bson> pairOf(Bson filter, Bson update) { return new Pair<>(filter, update); }

  private static class DocumentUpdates {
    Map<String, FieldUpdate> fieldUpdatesMap = new HashMap<>();
    long grVersion = -1;

    void addUpdate(long grVersion, String field, FieldUpdate fieldUpdate) {
      if (grVersion > this.grVersion) {
        this.grVersion = grVersion;
      }
      FieldUpdate existingUpdate = fieldUpdatesMap.get(field);
      if (existingUpdate == null || existingUpdate.grVersion <= grVersion) {
        insertAndUpdate(field, fieldUpdate);
      }
    }

    private void insertAndUpdate(String key, FieldUpdate fieldUpdate) {
      // parent keys are given higher priority, only parent updates are retained.
      if (isChildKeyOfExistingKeys(key)) {
        return; // Skip insert
      }
      fieldUpdatesMap.put(key, fieldUpdate);
    }

    private boolean isChildKeyOfExistingKeys(String key) {
      return fieldUpdatesMap.keySet().stream().anyMatch(existingKey -> isChildField(existingKey, key));
    }

    private boolean isChildField(String parentField, String field) { return field.startsWith(parentField + "."); }
  }

  @AllArgsConstructor
  private static class FieldUpdate {
    Object value;
    long grVersion;
    OP op;
  }

  public static enum OP {
    SET,
    UNSET;
  }
}
