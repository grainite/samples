package org.samples.mongocdc;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

public class Verify {

  private static final String ON_PREM_URI = "mongodb://localhost:27017";
  private static final String CLOUD_URI = "mongodb://localhost:27018";

  private static final String DB_NAME = "test";
  private static final String COLLECTION_NAME = "customers";

  public static void main(String[] args) {
    MongoClient onPrem = MongoClients.create(ON_PREM_URI);
    MongoClient cloud = MongoClients.create(CLOUD_URI);

    MongoDatabase onPremDB = onPrem.getDatabase(DB_NAME);
    MongoDatabase cloudDB = cloud.getDatabase(DB_NAME);

    MongoCollection<Document> onPremCollection = onPremDB.getCollection(COLLECTION_NAME);
    MongoCollection<Document> cloudCollection = cloudDB.getCollection(COLLECTION_NAME);

    long numOnPremDocs = onPremCollection.countDocuments();
    long numCloudDocs = cloudCollection.countDocuments();

    if (numOnPremDocs != numCloudDocs) {
      System.out.println("!!! Document count mismatch !!!");
      System.out.println("On-prem: " + numOnPremDocs);
      System.out.println("Cloud: " + numCloudDocs);
      System.exit(1);
    }

    MongoCursor<Document> onPremCursor = onPremCollection.find().iterator();

    while (onPremCursor.hasNext()) {
      Document onPremDoc = onPremCursor.next();
      ObjectId objectId = onPremDoc.getObjectId("_id");
      System.out.println(objectId);
      onPremDoc.remove("_grVersion");
      Document cloudDoc = cloudCollection.find(onPremDoc).first();
      if (cloudDoc == null) {
        System.out.println(String.format("!!! Mismatch on document %s !!!", objectId));
        System.exit(1);
      }
    }

    System.out.println("Success!");
  }
}
