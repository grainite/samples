package org.samples.mongocdc;

import com.github.javafaker.Faker;
import com.grainite.samples.loadgen.Throttler;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.UpdateOptions;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONObject;

public class Client {

  private static Random docRandom = new Random(42);
  private static Random random = new Random();
  private static Faker faker = new Faker(random);
  private static List<ObjectId> ids = new ArrayList<>();

  public static void main(String[] args) {

    if (args.length != 5) {
      System.out.println(
          "Usage: <mongo connection string> <database name> <collection name> <num events to send> <target event rate>");
      System.exit(1);
    }

    String mongoConnectionString = args[0];
    String database = args[1];
    String collectionName = args[2];
    int numEventsToSend = Integer.parseInt(args[3]);
    int targetEventRate = Integer.parseInt(args[4]);

    int numberOfDocs = 100;

    for (int i = 0; i < numberOfDocs; i++) {
      ids.add(getRandomObjectId());
    }

    int eventsSent = 0;

    MongoClient client = MongoClients.create(mongoConnectionString);
    MongoCollection<Document> collection = client.getDatabase(database).getCollection(collectionName);

    Throttler throttler = new Throttler(targetEventRate);

    while (eventsSent < numEventsToSend) {

      ObjectId id = getRandomInsertedDocumentId();
      Document filter = new Document("_id", id);
      Document update = new Document("$set", Document.parse(getRandomJson()));
      collection.updateOne(filter, update, new UpdateOptions().upsert(true));

      throttler.throttle(1);
      eventsSent++;
    }

    client.close();

    System.out.printf("\n==== Completed Performing %s OPs ====\n", eventsSent);
  }

  private static ObjectId getRandomInsertedDocumentId() { return ids.get(random.nextInt(ids.size())); }

  private static String getRandomJson() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("name", faker.name().fullName());
    jsonObject.put("email", faker.internet().emailAddress());
    JSONArray jsonArray = new JSONArray();
    for (int i = 0; i < 3; i++) {
      JSONObject innerObject = new JSONObject();
      innerObject.put("city", faker.address().city());
      innerObject.put("country", faker.address().country());
      jsonArray.put(innerObject);
    }
    jsonObject.put("addresses", jsonArray);
    jsonObject.put("payload", randomString(random.nextInt(1_000_000)));
    return jsonObject.toString();
  }

  private static String randomString(long size) {
    StringBuffer sb = new StringBuffer();
    for (long i = 0; i < size; i++) {
      int val = random.nextInt(80);
      sb.append((char)(' ' + val));

      // 2(") and 60(\) are escaped in json string, therefore resulting in extra characters.
      if (val == 2 || val == 60) {
        size -= 1;
      }
    }

    return sb.toString();
  }

  private static ObjectId getRandomObjectId() {
    byte[] bytes = new byte[12];
    docRandom.nextBytes(bytes);
    return new ObjectId(bytes);
  }
}
