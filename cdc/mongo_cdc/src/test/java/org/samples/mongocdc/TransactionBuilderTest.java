package org.samples.mongocdc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.grainite.gimpl.common.Pair;
import com.jsoniter.output.JsonStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.samples.mongocdc.TransactionBuilder.OP;

@RunWith(JUnit4.class)
public class TransactionBuilderTest {
  @Test
  public void basicTransactionBuilderTest() {
    TransactionBuilder transactionBuilder = new TransactionBuilder();
    String objId = JsonStream.serialize(Map.of("uuid", 1));
    transactionBuilder.addWrites("source1", objId, "name", "foo", OP.SET, 1);
    Map<String, List<Pair<Bson, Bson>>> transactions = transactionBuilder.buildTransactionsForAllSources();
    assertEquals(1, transactions.size());
    assertEquals(1, transactions.get("source1").size());
    assertEquals(
        1,
        transactions.get("source1").get(0).a.toBsonDocument().get("_id").asDocument().get("uuid").asInt32().intValue());
    assertEquals("foo", transactions.get("source1")
                            .get(0)
                            .b.toBsonDocument()
                            .get("$set")
                            .asDocument()
                            .get("name")
                            .asString()
                            .getValue()
                            .toString());

    assertEquals(1, transactions.get("source1")
                        .get(0)
                        .b.toBsonDocument()
                        .get("$set")
                        .asDocument()
                        .get(TransactionBuilder.GR_VERSION_KEY)
                        .asInt64()
                        .longValue());

    // Transactions should be cleared after buildTransactionsForAllSources() call.
    assertEquals(0, transactionBuilder.buildTransactionsForAllSources().size());
  }

  @Test
  public void objectIdParsingTest() {
    String integerId = "1234";
    String doubleId = "12.34";
    String stringId = "\"1234\"";
    String documentId = "{\"hi\" : \"kafka\", \"nums\" : [10.0, 100.0, 1000.0]}";
    String objectId = "{\"$oid\" : \"596e275826f08b2730779e1f\"}";

    Map<String, Class<?>> expectedTypes = Map.of(integerId, Integer.class, doubleId, Double.class, stringId,
                                                 String.class, documentId, Document.class, objectId, ObjectId.class);

    for (Entry<String, Class<?>> entry : expectedTypes.entrySet()) {
      Object obj = TransactionBuilder.getObjectId(entry.getKey());
      assertTrue(obj.getClass() == entry.getValue());
    }
  }
}
