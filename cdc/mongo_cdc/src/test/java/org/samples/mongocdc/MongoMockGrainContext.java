package org.samples.mongocdc;

import static org.junit.Assert.assertEquals;

import com.grainite.api.Key;
import com.grainite.api.Value;
import com.grainite.api.context.CounterParam;
import com.grainite.test.MockGrainContext;
import java.util.HashMap;
import java.util.Map;

public class MongoMockGrainContext extends MockGrainContext

{
  public MongoMockGrainContext(String appName, String tableName, Key key, Map<String, String> config) {
    super(appName, tableName, key, config);
  }

  Map<Key, Value> indexEntries = new HashMap<>();

  @Override
  public void insertIndexEntry(Key key, Value value) {
    indexEntries.put(key, value);
  }

  @Override
  public void deleteIndexEntry(Key key, Value oldValue) {
    assertEquals(indexEntries.remove(key), oldValue);
  }

  @Override
  public void updateIndexEntry(Key key, Value oldValue, Value newValue) {
    assertEquals(indexEntries.remove(key), oldValue);
    indexEntries.put(key, newValue);
  }

  Map<String, MyCounter> countersMap = new HashMap<>();

  @Override
  public Counter counter(String name, CounterParam... params) {
    if (countersMap.containsKey(name)) {
      return countersMap.get(name);
    }

    MyCounter counter = new MyCounter();
    countersMap.put(name, counter);
    return counter;
  }

  public static class MyCounter implements Counter {
    public int val = 0;

    @Override
    public void inc() {
      this.val++;
    }

    @Override
    public void inc(long incrementBy) {
      this.val += incrementBy;
    }
  }
}
