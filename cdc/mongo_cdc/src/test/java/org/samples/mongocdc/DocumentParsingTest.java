package org.samples.mongocdc;

import static org.junit.Assert.assertEquals;

import java.util.Map;
import java.util.Set;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class DocumentParsingTest {

  @Test
  public void testBasicParsing() {
    String jsonStringWithNestedObjs =
        "{ \"_id\" : { \"$oid\" : \"6463c61241bfdd4ffc24383b\" }, \"long\" : { \"$numberLong\" : \"123323232\" }, \"time\" : { \"$timestamp\" : { \"t\" : 1565545664, \"i\" : 1 } }, \"nestedObject\" : { \"nestedKey1\" : \"nestedValue1\", \"nestedKey2\" : \"nestedValue2\", \"nestedTimestamp\" : { \"$timestamp\" : { \"t\" : 1621200000, \"i\" : 2 } } } }";

    Set<String> expectedKeys = Set.of("long", "time", "nestedObject", "nestedObject.nestedKey1",
                                      "nestedObject.nestedKey2", "nestedObject.nestedTimestamp");

    Map<String, Object> keys = Utils.buildFlatKeys(jsonStringWithNestedObjs);

    assertEquals(expectedKeys, keys.keySet());

    Document doc =
        new Document("_id", new ObjectId()).append("nest", new Document("nested", new Document("mid", "value")));

    String jsonStringWithDoubleNestedObjs = doc.toJson();
    expectedKeys = Set.of("nest.nested.mid");

    keys = Utils.buildFlatKeys(jsonStringWithDoubleNestedObjs);

    expectedKeys = Set.of("nest", "nest.nested", "nest.nested.mid");
    keys = Utils.buildFlatKeys(jsonStringWithDoubleNestedObjs);

    assertEquals(expectedKeys, keys.keySet());

    // String arr = "{\"val\": [1, 2, 4]}";
    String arr = "{\"val.1\": 2}";

    System.out.println(Utils.buildFlatKeys(arr));
    System.out.println(Utils.buildFlatKeys(arr));
  }
}