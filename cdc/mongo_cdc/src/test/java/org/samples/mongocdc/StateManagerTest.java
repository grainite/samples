package org.samples.mongocdc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.grainite.api.Key;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.samples.mongocdc.StateManager.DeleteKeyPartFoundException;
import org.samples.mongocdc.StateManager.KeyState;
import org.samples.mongocdc.StateManager.MaxKeyPart;

@RunWith(JUnit4.class)
public class StateManagerTest {
  MongoMockGrainContext mockGrainContext;

  @Before
  public void testPrep() {
    mockGrainContext = new MongoMockGrainContext("test_app", "test_table", Key.of("test_key"), null);
  }

  @Test
  public void basicTest() throws StateManager.DeleteKeyPartFoundException {
    StateManager stateManager = new StateManager(mockGrainContext);
    stateManager.insertKey("test_key", 1, false, null);
    stateManager.persistState();

    mockGrainContext.mapKeyExists(0, Key.of("test_object_id"));

    // Test persistence.
    stateManager = new StateManager(mockGrainContext);
    MaxKeyPart res = stateManager.getMaxKeyPart("test_key", false);
    assertNotNull(res);
    assertEquals(Long.valueOf(1), res.getMaxVersion());
    assertTrue(res.getIsEchoPending().isEmpty());

    // Test non existent key.
    res = stateManager.getMaxKeyPart("test_key1", false);
    assertNull(res);
  }

  @Test
  public void testKeyParts() {
    // Top level field.
    String key = "key1";
    List<String> expectedParts = List.of("key1");
    assertEquals(expectedParts, StateManager.getKeyParts(key));

    // Nested map.
    key = "key1.key2.key3";
    expectedParts = List.of("key1", "key1.key2", "key1.key2.key3");
    assertEquals(expectedParts, StateManager.getKeyParts(key));

    // Array index.
    key = "key1.0";
    expectedParts = List.of("key1", "key1.0");
    assertEquals(expectedParts, StateManager.getKeyParts(key));

    // Array element.
    key = "key1.0.subkey2";
    expectedParts = List.of("key1", "key1.0", "key1.0.subkey2");
    assertEquals(expectedParts, StateManager.getKeyParts(key));
  }

  @Test
  public void testMaxKeyPart() throws DeleteKeyPartFoundException {
    StateManager stateManager = new StateManager(mockGrainContext);
    String complexKey = "key1.key2.key3";
    String source = "source1";
    stateManager.insertKey(complexKey, 1, false, Map.of(source, true));

    KeyState keyState = stateManager.getKey(complexKey);
    assertNotNull(keyState);
    assertTrue(keyState.getIsEchoPending().get(source));
    assertEquals(1, keyState.getVersion());

    MaxKeyPart res = stateManager.getMaxKeyPart(complexKey, false);
    assertNotNull(res);
    assertEquals(Long.valueOf(1), res.getMaxVersion());
    assertTrue(res.getIsEchoPendingForSource(source));

    // Test non existent key part.
    res = stateManager.getMaxKeyPart("key1", false);
    assertNull(res);

    // Test multiple key parts.
    stateManager.insertKey("key1", 1, false, Map.of(source, true));
    stateManager.insertKey("key1.key2", 2, false, Map.of(source, false));

    res = stateManager.getMaxKeyPart(complexKey, false);
    assertNotNull(res);
    // Max version should be 2 due to key1.key2.
    assertEquals(Long.valueOf(2), res.getMaxVersion());
    assertTrue(res.getIsEchoPendingForSource(source));
  }

  @Test
  public void testArrayInserts() throws DeleteKeyPartFoundException {
    StateManager stateManager = new StateManager(mockGrainContext);
    String arrayKey1 = "key1.0.subkey1";
    String arrayKey2 = "key1.0.subkey2";

    stateManager.insertKey(arrayKey1, 1, false, Map.of("source1", true));
    stateManager.insertKey(arrayKey2, 3, false, Map.of("source2", false));

    assertNotNull(stateManager.getKey(arrayKey1));
    assertNotNull(stateManager.getKey(arrayKey2));

    MaxKeyPart res = stateManager.getMaxKeyPart("key1.0", false);
    assertNull(res);

    res = stateManager.getMaxKeyPart(arrayKey1, false);
    assertNotNull(res);
    // Scan should have picked up key1.0.
    assertEquals(Long.valueOf(1), res.getMaxVersion());

    res = stateManager.getMaxKeyPart(arrayKey2, false);
    assertNotNull(res);
    // Scan should have picked up key1.0.subkey2.
    assertEquals(Long.valueOf(3), res.getMaxVersion());

    res = stateManager.getMaxKeyPart("key1.0.subkey1.subsubkey", false);
    assertNotNull(res);
    // Scan should have picked up key1.0.subkey1.
    assertEquals(Long.valueOf(1), res.getMaxVersion());

    res = stateManager.getMaxKeyPart("key1.0.subkey2.subsubkey1", false);
    assertNotNull(res);
    // Scan should have picked up key1.0.subkey2.
    assertEquals(Long.valueOf(3), res.getMaxVersion());
    assertFalse(res.getIsEchoPendingForSource("source2"));
  }

  @Test
  public void testKeyDelete() throws DeleteKeyPartFoundException {
    StateManager stateManager = new StateManager(mockGrainContext);
    String key = "key1.key2";

    stateManager.insertKey(key, 1, true, Map.of("source1", true));

    KeyState keyState = stateManager.getKey(key);
    assertNotNull(keyState);
    assertTrue(keyState.isDelete());
    assertEquals(1, keyState.getVersion());

    MaxKeyPart res = stateManager.getMaxKeyPart(key, false);
    assertNotNull(res);
    assertTrue(res.getIsEchoPendingForSource("source1"));

    boolean wasExceptionThrown = false;

    try {
      stateManager.getMaxKeyPart(key, true);
    } catch (DeleteKeyPartFoundException e) {
      wasExceptionThrown = true;
    }

    assertTrue(wasExceptionThrown);
  }

  @Test
  public void testEchoes() {
    StateManager stateManager = new StateManager(mockGrainContext);
    String key = "key1";

    stateManager.insertKey(key, 1, false, Map.of("source1", true, "source2", false));

    KeyState keyState = stateManager.getKey(key);
    assertNotNull(keyState);
    assertTrue(keyState.isEchoPending("source1"));
    assertFalse(keyState.isEchoPending("source2"));

    // This should be ignored as echo version is stale (0).
    stateManager.handleEcho("source1", 0);
    keyState = stateManager.getKey(key);
    assertNotNull(keyState);
    assertTrue(keyState.isEchoPending("source1"));
    assertFalse(keyState.isEchoPending("source2"));

    // This should be ignored as echo for source2 is already received..
    stateManager.handleEcho("source2", 1);
    keyState = stateManager.getKey(key);
    assertNotNull(keyState);
    assertTrue(keyState.isEchoPending("source1"));
    assertFalse(keyState.isEchoPending("source2"));

    // This should result in all echoes being received.
    // Since all echoes are received, the key should be deleted.
    stateManager.handleEcho("source1", 1);
    keyState = stateManager.getKey(key);
    assertNull(keyState);

    // Test persistence.
    stateManager.persistState();

    stateManager = new StateManager(mockGrainContext);
    keyState = stateManager.getKey(key);
    assertNull(keyState);
  }
}
