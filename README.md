# samples
Sample applications exercising the Grainite API (https://www.grainite.com).

## Getting Started
Follow the instructions in [`quickstart.md`](./quickstart.md) to get up and running.

## Usage
- Java: To build and install, run `./mvnw clean compile package` and then follow the README in wordcount or userflows for instructions on deploying the application to a Grainite server.
- Python: To install the Grainite python package, run `pip install grainite-cl --index-url https://gitlab.com/api/v4/projects/41132986/packages/pypi/simple` and then follow the README in helloPython for instructions on deploying the application to a Grainite server.

## Updates
- To update to the latest version of the samples repo, including the latest version of gx, simply run `git pull`
- For Java applications, you'll also need to rerun `./mvnw clean compile package`

## Modules

- wordcount/:      sample application in Java
- helloPython/: sample application in Python
- userflows/:      sample Java application demonstrating tracking customer state through multiple channels

- gx-deps/:        contains a maven pom to access the Grainite command line utility gx. Run with $SAMPLES_ROOT/gx

## Support
For any questions, please reach out to support@grainite.com or eval@grainite.com if you're a trial user.
