Note: This is an abbreviated version of the Grainite documentation. For the full documentation please visit [gitbook.grainite.com](https://gitbook.grainite.com).

# Overview
When building real-time applications, architects and designers have to take into consideration several factors, including

- Stream ingestion and storage
- Stream processing, which may include multi-step workflows and invoking external functions/microservices
- Database storage, and caching layers to support high throughput read-modify-write operations
- Integration with sinks which can be used for long-range analytics
- Service Level Agreements related to Latencies, Durability, and Availability 
- Security of data at rest and in motion
- Deployment considerations

Grainite is a **converged streaming application platform** that brings together the ability to stream and store events in topics, orchestrate workflows, read and process events in real-time, as they arrive, as well as store (and later query) any processing outputs into its built-in strongly consistent database.

The document provides an overview of how Grainite may be used to develop an application that operates on real-time streaming, as well as, static data, and how the Grainite model makes it significantly easier to develop modern applications for the Cloud.



# Grainite Applications
An application developed in Grainite is composed of **Topics**, **Tables** (that contain **Grains**), and **Action Handlers** (associated with Grains).

Grainite stores data in Topics and Tables. **Topics are collections of Events. Tables are collections of Grains.** Topics are similar to Topics in other queue-based systems like Kafka, IBM MQ, and others. Tables are similar to tables in most databases, in that they allow structured data to be stored, mutated, and accessed efficiently. Tables contain Grains, and Grains have attached Action Handlers for processing requests to the Grain.

Developers provide the list of Topics, Tables, Grains, their Action Handlers, and the relationships between them in a config file that is submitted to the Grainite server to load a new version of the application.

At this time, Grainite exposes a **Java API** for Client Applications and for implementing Grain Action Handlers. **Additional language support is coming for Python, Javascript, and other languages.**

The concepts of Topics, Events, Tables, Grains, and Action Handlers are further described in the sections below.

![Application Architecture w/ Grainite](./resources/app_architecture.png "Application Architecture w/ Grainite")



# Topics and Events
A Topic is used to store Events. Clients can send (or produce) Events to be stored in Topics and other clients can pull (or consume) Events from Topics. **Topics may have multiple producers and consumers.**

**Events have the property that they are immutable.** Usually, Events represent a user interaction, a stock ticker, an inventory change, or other such business interactions that need to be stored and accessed at a later point.

**Events stored inside topics may have a key** (or multiple keys, in the future). When a primary (or first) key is provided, the **Topic provides ordering guarantees by that key**. In other words, when reading from the Topic, events for a particular key will be returned in the order they were added to the system.



# Tables and Grains
A Table is used to store Grains. **Grains include both data (similar to a row in a database table), but also may include some behaviors represented as Grain Action Handlers**. Grains may be used to store data like Shopping Carts, Customer data, Product Catalogs, and other such business entities.

Grains may **subscribe to topics** (to receive new events and process them), **receive messages from other Grains** or external applications, as well as, **post messages to other Grains**.

 ![Grainite Table](./resources/grainite_table.png "Grainite Table")

# Grain Action Handlers
**Grain Action Handlers are user-provided code used to represent behaviors on a Grain.** For instance, when a Grain subscribes to a Topic, an Action Handler provided by the developer will be invoked for (each batch of) events so that it may extract the relevant data from the events, process it, maybe make external calls, save any data derived from the events into its own storage, and send any messages to other Grains or Topics.

Grain Action Handlers are invoked directly within the Grainite Cluster. In a future release, Grainite will be able to invoke remote Action Handlers that might be installed as REST endpoints, gRPC service endpoints or even serverless functions like AWS Lambdas, Azure Durable Functions, or equivalent.

**Each Grain may have multiple action handlers, potentially implemented in multiple different languages.** For example, one Action Handler might respond to Shopping Cart events, while another Action Handler might respond to Shipping events.

Grainite provides a **guarantee that only one action handler is active at any time for a given Grain**.



# Examples of Grainite’s Simple API
Below are a few examples to highlight the simplicity of the unified API that Grainite offers which covers all of the requirements that a real-time / streaming application needs from the underlying infrastructure.

## Appending Events to a Topic
Events may be appended to a Topic with a code snippet like below. Ordering guarantees are provided by Grainite on a per-key basis, unlike other systems that order on a per-shard basis across unrelated keys.

```
Topic ordersTopic = grainite.getTopic("order_app", "orders_topic");
ordersTopic.append(new Event(Key.of(order.id), Value.ofObject(order)));
```

## Consume Events from a Topic
Events are consumed as they arrive. Topics and Consumers are connected via configuration (see app.yaml below), and Grainite will automatically invoke the configured event handler when an event is available. Grainite attempts to schedule as many event handlers in parallel as possible - up to the number of unique keys. 

```
// This handler is invoked for one event for one particular user. The 
// code has exclusive access to the underlying data without locking.

public ActionResult handleAction(Action action, GrainContext context) {
  // Get the order from the action
  Order o = ((TopicEvent)action).getPayload().asType(Order.class);

  // Perform some transformations and run some business logic.
  ...

  return ActionResult.success(action);
}
```

## Access and update entity state and history
Grainite guarantees that there can be no more than one handler processing an event for a given key. This allows the handler to access and update any directly associated state with the key without needing to lock or worry about concurrent updates. Both access and updates are done synchronously and at low latency.

```java
  // access the user summary or arbitrary user history
  User user = context.getValue().asType(User.class);
  Iterator<KeyValue> lastOneDayOrders = 
    context.mapQuery(
      MapQuery.newBuilder()
        .mapName("processed_orders")
        .setRange(Key.of(getOneDayAgoTime()), true, Key.maxKey(), true));

  // Perform some transformations and run some business logic.
  ...

  // Store data back into Grain.
  context.setValue(Value.of(newValue));
  context.mapPut("processed_orders", Key.of(System.currentTimeMillis()), 
    Value.ofObject(order));
```

## Access and update other entities and history
Grainite provides messaging to communicate with other entities or handlers. The messaging backbone supports exactly-once processing - and a handler can send an exactly-once message to any number of other entities. For example, an order handler may send an exactly-once message to the inventory handler for each product in order, to reduce the inventory level.

```
for (LineItem l: order.items) {
  GrainOp.Invoke op = new GrainOp.Invoke("reduceInventory",
      Value.of(l.product.quantity));
  context.sendToGrain("inventory", Value.of(l.product.id), op, null);
}
```

## Regular DB query and updates
Grainite supports regular querying and updates by external clients. This is enabled with gRPC APIs, but also available through various client language libraries. Clients may perform Topic, Table, or Workflow actions, and invoke handlers directly using the Grainite API.

```
Table userTable = grainite.getTable("order_app", "user_table");
Grain userGrain = userTable.get(Value.of(userId));
// Get the summary for the user, and fetch the last 2 orders
User user = userGrain.getValue().asType(User.class)
Iterator<KeyValue> lastTwoOrders = userGrain.mapGetRange(...);
// Update any fields in the database and write back the update
...
userGrain.setValue(Value.ofObject(user));
```

## Configuration with app.yaml
Grainite applications are configured with a yaml configuration which defines the Topics, Tables, Subscriptions and Actions (event handlers). A simple example is shown below. 

```
app_name: order_app

# define the topics in the application. Only one topic for clients to
# append new orders into.

topics:
- topic_name: orders_topic
  key_type: string

# defines the tables in the application. In this case a table that keeps
# the state of the user, and another that keeps inventory levels for 
# each product.

tables:
- table_name: user_table
  key_type: string
  action_classes:
  - name: IncomingOrderHandler
    class_name: org.grainite.samples.IncomingOrderHandler
    actions:
    - action_name: handleOrder
    subscriptions:
    - subscription_name: newOrders
      topic_name: orders_topic
      topic_key: order_id

- table_name: inventory
  ...
```

## Some other key capabilities
Grainite supports executing timer-based actions, orchestration of multi-step workflows at scale, and integrates using standard connector interfaces with existing infrastructure e.g. stream sources and data warehouses. Grainite has built-in monitoring to understand how events are flowing through the system and where processing delays might occur.

See the javadoc (included in the [Grainite samples repo](https://gitlab.com/grainite/samples/-/tree/main/)) for more details on Grainite’s simple and powerful API. Our full documentation which covers general and advanced capabilities is available at [gitbook.grainite.com](https://gitbook.grainite.com).

# Summary
Grainite provides a novel approach and architecture to develop online applications by unifying a database custom-built for event processing applications, along with event storage and a stream processing engine. Some of the unique benefits of using Grainite to build online applications are: 

- **Development and operational simplicity:**  Allow developers to focus on building distributed applications easily and use optimal infrastructure resources to run their applications.
- **Flexible deployment:** Deploy in public (AWS, Azure, and GCP) or private clouds using Kubernetes in minutes.
- **Easy adoption:** Leverage Grainite to build brownfield (compatible with existing components) or new/greenfield applications. 

